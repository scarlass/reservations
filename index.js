(function (isNw) {
  const PORT = 8080;
  const url = 'http://localhost:' + PORT;

  if (isNw) {
    const nmain = nw.Window.open(url, {}, function (win) {
      win.window.HELLO = 'HELLO WORLD!'
    })

  }
  else {
    const el = require('electron');
    let app = null;
    function createWindow() {
      app = new el.BrowserWindow({});
      app.webContents.openDevTools({ mode: 'detach' })
      app.loadURL(url)
    }

    el.app.on('ready', createWindow);
    el.app.on('quit', function (evt) { })
  }




})(typeof globalThis.nw !== 'undefined');