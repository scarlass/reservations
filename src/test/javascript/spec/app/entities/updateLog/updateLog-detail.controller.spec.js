'use strict';

describe('Controller Tests', function() {

    describe('UpdateLog Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockUpdateLog, MockReservation, MockRoom, MockItem;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockUpdateLog = jasmine.createSpy('MockUpdateLog');
            MockReservation = jasmine.createSpy('MockReservation');
            MockRoom = jasmine.createSpy('MockRoom');
            MockItem = jasmine.createSpy('MockItem');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'UpdateLog': MockUpdateLog,
                'Reservation': MockReservation,
                'Room': MockRoom,
                'Item': MockItem
            };
            createController = function() {
                $injector.get('$controller')("UpdateLogDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'reservationsApp:updateLogUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
