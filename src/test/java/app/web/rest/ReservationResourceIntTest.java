package app.web.rest;

import app.Application;
import app.domain.Reservation;
import app.repository.ReservationRepository;
import app.service.ReservationService;
import app.web.rest.dto.ReservationDTO;
import app.web.rest.mapper.ReservationMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ReservationResource REST controller.
 *
 * @see ReservationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ReservationResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));

    private static final String DEFAULT_USER_LOGIN = "AAAAA";
    private static final String UPDATED_USER_LOGIN = "BBBBB";

    private static final ZonedDateTime DEFAULT_USAGE_TIME_START = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_USAGE_TIME_START = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_USAGE_TIME_START_STR = dateTimeFormatter.format(DEFAULT_USAGE_TIME_START);

    private static final ZonedDateTime DEFAULT_USAGE_TIME_END = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_USAGE_TIME_END = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_USAGE_TIME_END_STR = dateTimeFormatter.format(DEFAULT_USAGE_TIME_END);
    
    private static final String DEFAULT_DETAILS = "AAAAA";
    private static final String UPDATED_DETAILS = "BBBBB";

    private static final ZonedDateTime DEFAULT_TIME_INPUT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_TIME_INPUT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_TIME_INPUT_STR = dateTimeFormatter.format(DEFAULT_TIME_INPUT);

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    @Inject
    private ReservationRepository reservationRepository;

    @Inject
    private ReservationMapper reservationMapper;

    @Inject
    private ReservationService reservationService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restReservationMockMvc;

    private Reservation reservation;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReservationResource reservationResource = new ReservationResource();
        ReflectionTestUtils.setField(reservationResource, "reservationService", reservationService);
        ReflectionTestUtils.setField(reservationResource, "reservationMapper", reservationMapper);
        this.restReservationMockMvc = MockMvcBuilders.standaloneSetup(reservationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        reservation = new Reservation();
        reservation.setUserLogin(DEFAULT_USER_LOGIN);
        reservation.setUsageTimeStart(DEFAULT_USAGE_TIME_START);
        reservation.setUsageTimeEnd(DEFAULT_USAGE_TIME_END);
        reservation.setDetails(DEFAULT_DETAILS);
        reservation.setTimeInput(DEFAULT_TIME_INPUT);
        reservation.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createReservation() throws Exception {
        int databaseSizeBeforeCreate = reservationRepository.findAll().size();

        // Create the Reservation
        ReservationDTO reservationDTO = reservationMapper.reservationToReservationDTO(reservation);

        restReservationMockMvc.perform(post("/api/reservations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(reservationDTO)))
                .andExpect(status().isCreated());

        // Validate the Reservation in the database
        List<Reservation> reservations = reservationRepository.findAll();
        assertThat(reservations).hasSize(databaseSizeBeforeCreate + 1);
        Reservation testReservation = reservations.get(reservations.size() - 1);
        assertThat(testReservation.getUserLogin()).isEqualTo(DEFAULT_USER_LOGIN);
        assertThat(testReservation.getUsageTimeStart()).isEqualTo(DEFAULT_USAGE_TIME_START);
        assertThat(testReservation.getUsageTimeEnd()).isEqualTo(DEFAULT_USAGE_TIME_END);
        assertThat(testReservation.getDetails()).isEqualTo(DEFAULT_DETAILS);
        assertThat(testReservation.getTimeInput()).isEqualTo(DEFAULT_TIME_INPUT);
        assertThat(testReservation.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void checkUsageTimeStartIsRequired() throws Exception {
        int databaseSizeBeforeTest = reservationRepository.findAll().size();
        // set the field null
        reservation.setUsageTimeStart(null);

        // Create the Reservation, which fails.
        ReservationDTO reservationDTO = reservationMapper.reservationToReservationDTO(reservation);

        restReservationMockMvc.perform(post("/api/reservations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(reservationDTO)))
                .andExpect(status().isBadRequest());

        List<Reservation> reservations = reservationRepository.findAll();
        assertThat(reservations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUsageTimeEndIsRequired() throws Exception {
        int databaseSizeBeforeTest = reservationRepository.findAll().size();
        // set the field null
        reservation.setUsageTimeEnd(null);

        // Create the Reservation, which fails.
        ReservationDTO reservationDTO = reservationMapper.reservationToReservationDTO(reservation);

        restReservationMockMvc.perform(post("/api/reservations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(reservationDTO)))
                .andExpect(status().isBadRequest());

        List<Reservation> reservations = reservationRepository.findAll();
        assertThat(reservations).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllReservations() throws Exception {
        // Initialize the database
        reservationRepository.saveAndFlush(reservation);

        // Get all the reservations
        restReservationMockMvc.perform(get("/api/reservations?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(reservation.getId().intValue())))
                .andExpect(jsonPath("$.[*].userLogin").value(hasItem(DEFAULT_USER_LOGIN.toString())))
                .andExpect(jsonPath("$.[*].usageTimeStart").value(hasItem(DEFAULT_USAGE_TIME_START_STR)))
                .andExpect(jsonPath("$.[*].usageTimeEnd").value(hasItem(DEFAULT_USAGE_TIME_END_STR)))
                .andExpect(jsonPath("$.[*].details").value(hasItem(DEFAULT_DETAILS.toString())))
                .andExpect(jsonPath("$.[*].timeInput").value(hasItem(DEFAULT_TIME_INPUT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    @Transactional
    public void getReservation() throws Exception {
        // Initialize the database
        reservationRepository.saveAndFlush(reservation);

        // Get the reservation
        restReservationMockMvc.perform(get("/api/reservations/{id}", reservation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(reservation.getId().intValue()))
            .andExpect(jsonPath("$.userLogin").value(DEFAULT_USER_LOGIN.toString()))
            .andExpect(jsonPath("$.usageTimeStart").value(DEFAULT_USAGE_TIME_START_STR))
            .andExpect(jsonPath("$.usageTimeEnd").value(DEFAULT_USAGE_TIME_END_STR))
            .andExpect(jsonPath("$.details").value(DEFAULT_DETAILS.toString()))
            .andExpect(jsonPath("$.timeInput").value(DEFAULT_TIME_INPUT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    public void getNonExistingReservation() throws Exception {
        // Get the reservation
        restReservationMockMvc.perform(get("/api/reservations/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReservation() throws Exception {
        // Initialize the database
        reservationRepository.saveAndFlush(reservation);

		int databaseSizeBeforeUpdate = reservationRepository.findAll().size();

        // Update the reservation
        reservation.setUserLogin(UPDATED_USER_LOGIN);
        reservation.setUsageTimeStart(UPDATED_USAGE_TIME_START);
        reservation.setUsageTimeEnd(UPDATED_USAGE_TIME_END);
        reservation.setDetails(UPDATED_DETAILS);
        reservation.setTimeInput(UPDATED_TIME_INPUT);
        reservation.setStatus(UPDATED_STATUS);
        ReservationDTO reservationDTO = reservationMapper.reservationToReservationDTO(reservation);

        restReservationMockMvc.perform(put("/api/reservations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(reservationDTO)))
                .andExpect(status().isOk());

        // Validate the Reservation in the database
        List<Reservation> reservations = reservationRepository.findAll();
        assertThat(reservations).hasSize(databaseSizeBeforeUpdate);
        Reservation testReservation = reservations.get(reservations.size() - 1);
        assertThat(testReservation.getUserLogin()).isEqualTo(UPDATED_USER_LOGIN);
        assertThat(testReservation.getUsageTimeStart()).isEqualTo(UPDATED_USAGE_TIME_START);
        assertThat(testReservation.getUsageTimeEnd()).isEqualTo(UPDATED_USAGE_TIME_END);
        assertThat(testReservation.getDetails()).isEqualTo(UPDATED_DETAILS);
        assertThat(testReservation.getTimeInput()).isEqualTo(UPDATED_TIME_INPUT);
        assertThat(testReservation.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteReservation() throws Exception {
        // Initialize the database
        reservationRepository.saveAndFlush(reservation);

		int databaseSizeBeforeDelete = reservationRepository.findAll().size();

        // Get the reservation
        restReservationMockMvc.perform(delete("/api/reservations/{id}", reservation.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Reservation> reservations = reservationRepository.findAll();
        assertThat(reservations).hasSize(databaseSizeBeforeDelete - 1);
    }
}
