package app.web.rest;

import app.Application;
import app.domain.UpdateLog;
import app.repository.UpdateLogRepository;
import app.service.UpdateLogService;
import app.web.rest.dto.UpdateLogDTO;
import app.web.rest.mapper.UpdateLogMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the UpdateLogResource REST controller.
 *
 * @see UpdateLogResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class UpdateLogResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));

    private static final String DEFAULT_MESSAGE = "AAAAA";
    private static final String UPDATED_MESSAGE = "BBBBB";

    private static final Integer DEFAULT_MESSAGE_TYPE = 1;
    private static final Integer UPDATED_MESSAGE_TYPE = 2;
    
    private static final String DEFAULT_ADDITIONAL_MESSAGE = "AAAAA";
    private static final String UPDATED_ADDITIONAL_MESSAGE = "BBBBB";

    private static final Integer DEFAULT_ENTITY_TYPE = 1;
    private static final Integer UPDATED_ENTITY_TYPE = 2;

    private static final ZonedDateTime DEFAULT_TIME_INPUT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_TIME_INPUT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_TIME_INPUT_STR = dateTimeFormatter.format(DEFAULT_TIME_INPUT);

    @Inject
    private UpdateLogRepository updateLogRepository;

    @Inject
    private UpdateLogMapper updateLogMapper;

    @Inject
    private UpdateLogService updateLogService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restUpdateLogMockMvc;

    private UpdateLog updateLog;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UpdateLogResource updateLogResource = new UpdateLogResource();
        ReflectionTestUtils.setField(updateLogResource, "updateLogService", updateLogService);
        ReflectionTestUtils.setField(updateLogResource, "updateLogMapper", updateLogMapper);
        this.restUpdateLogMockMvc = MockMvcBuilders.standaloneSetup(updateLogResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        updateLog = new UpdateLog();
        updateLog.setMessage(DEFAULT_MESSAGE);
        updateLog.setMessageType(DEFAULT_MESSAGE_TYPE);
        updateLog.setAdditionalMessage(DEFAULT_ADDITIONAL_MESSAGE);
        updateLog.setEntityType(DEFAULT_ENTITY_TYPE);
        updateLog.setTimeInput(DEFAULT_TIME_INPUT);
    }

    @Test
    @Transactional
    public void createUpdateLog() throws Exception {
        int databaseSizeBeforeCreate = updateLogRepository.findAll().size();

        // Create the UpdateLog
        UpdateLogDTO updateLogDTO = updateLogMapper.updateLogToUpdateLogDTO(updateLog);

        restUpdateLogMockMvc.perform(post("/api/updateLogs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updateLogDTO)))
                .andExpect(status().isCreated());

        // Validate the UpdateLog in the database
        List<UpdateLog> updateLogs = updateLogRepository.findAll();
        assertThat(updateLogs).hasSize(databaseSizeBeforeCreate + 1);
        UpdateLog testUpdateLog = updateLogs.get(updateLogs.size() - 1);
        assertThat(testUpdateLog.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testUpdateLog.getMessageType()).isEqualTo(DEFAULT_MESSAGE_TYPE);
        assertThat(testUpdateLog.getAdditionalMessage()).isEqualTo(DEFAULT_ADDITIONAL_MESSAGE);
        assertThat(testUpdateLog.getEntityType()).isEqualTo(DEFAULT_ENTITY_TYPE);
        assertThat(testUpdateLog.getTimeInput()).isEqualTo(DEFAULT_TIME_INPUT);
    }

    @Test
    @Transactional
    public void getAllUpdateLogs() throws Exception {
        // Initialize the database
        updateLogRepository.saveAndFlush(updateLog);

        // Get all the updateLogs
        restUpdateLogMockMvc.perform(get("/api/updateLogs?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(updateLog.getId().intValue())))
                .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
                .andExpect(jsonPath("$.[*].messageType").value(hasItem(DEFAULT_MESSAGE_TYPE)))
                .andExpect(jsonPath("$.[*].additionalMessage").value(hasItem(DEFAULT_ADDITIONAL_MESSAGE.toString())))
                .andExpect(jsonPath("$.[*].entityType").value(hasItem(DEFAULT_ENTITY_TYPE)))
                .andExpect(jsonPath("$.[*].timeInput").value(hasItem(DEFAULT_TIME_INPUT_STR)));
    }

    @Test
    @Transactional
    public void getUpdateLog() throws Exception {
        // Initialize the database
        updateLogRepository.saveAndFlush(updateLog);

        // Get the updateLog
        restUpdateLogMockMvc.perform(get("/api/updateLogs/{id}", updateLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(updateLog.getId().intValue()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.messageType").value(DEFAULT_MESSAGE_TYPE))
            .andExpect(jsonPath("$.additionalMessage").value(DEFAULT_ADDITIONAL_MESSAGE.toString()))
            .andExpect(jsonPath("$.entityType").value(DEFAULT_ENTITY_TYPE))
            .andExpect(jsonPath("$.timeInput").value(DEFAULT_TIME_INPUT_STR));
    }

    @Test
    @Transactional
    public void getNonExistingUpdateLog() throws Exception {
        // Get the updateLog
        restUpdateLogMockMvc.perform(get("/api/updateLogs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUpdateLog() throws Exception {
        // Initialize the database
        updateLogRepository.saveAndFlush(updateLog);

		int databaseSizeBeforeUpdate = updateLogRepository.findAll().size();

        // Update the updateLog
        updateLog.setMessage(UPDATED_MESSAGE);
        updateLog.setMessageType(UPDATED_MESSAGE_TYPE);
        updateLog.setAdditionalMessage(UPDATED_ADDITIONAL_MESSAGE);
        updateLog.setEntityType(UPDATED_ENTITY_TYPE);
        updateLog.setTimeInput(UPDATED_TIME_INPUT);
        UpdateLogDTO updateLogDTO = updateLogMapper.updateLogToUpdateLogDTO(updateLog);

        restUpdateLogMockMvc.perform(put("/api/updateLogs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updateLogDTO)))
                .andExpect(status().isOk());

        // Validate the UpdateLog in the database
        List<UpdateLog> updateLogs = updateLogRepository.findAll();
        assertThat(updateLogs).hasSize(databaseSizeBeforeUpdate);
        UpdateLog testUpdateLog = updateLogs.get(updateLogs.size() - 1);
        assertThat(testUpdateLog.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testUpdateLog.getMessageType()).isEqualTo(UPDATED_MESSAGE_TYPE);
        assertThat(testUpdateLog.getAdditionalMessage()).isEqualTo(UPDATED_ADDITIONAL_MESSAGE);
        assertThat(testUpdateLog.getEntityType()).isEqualTo(UPDATED_ENTITY_TYPE);
        assertThat(testUpdateLog.getTimeInput()).isEqualTo(UPDATED_TIME_INPUT);
    }

    @Test
    @Transactional
    public void deleteUpdateLog() throws Exception {
        // Initialize the database
        updateLogRepository.saveAndFlush(updateLog);

		int databaseSizeBeforeDelete = updateLogRepository.findAll().size();

        // Get the updateLog
        restUpdateLogMockMvc.perform(delete("/api/updateLogs/{id}", updateLog.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<UpdateLog> updateLogs = updateLogRepository.findAll();
        assertThat(updateLogs).hasSize(databaseSizeBeforeDelete - 1);
    }
}
