declare var ngApp: angular.IModule;
declare function isClass(fn: Function): boolean;
declare function UppercaseFirstLetter(text: string): string;
declare const log: typeof console.log;

namespace Rv {
  enum StatusCode {
    PENDING = 0,
    ACCEPTED = 1,
    FINISH = 2,
    CANCELED = 3,
  }
  enum EntityType {
    Reservation = 0,
    Room = 1,
    Item = 2,
  }
  enum MessageType {
    UPDATED = 0,
    CREATE = 1,
    DELETE = 2
  }
  var Status: Status[];

  interface Status {
    id: number;
    text: string;
  }

  interface BaseDB {
    id: number;
    userInput: string;
    timeInput: string;
  }

  interface ReservationDB extends BaseDB {
    userId: number;
    userLogin: string;
    details: string;
    status: number;
    usageTimeStart: string;
    usageTimeEnd: string;

    userInput?: undefined;
    roomId?: number;
    roomUpdateInfo?: string;
  }

  interface CustomerDB extends BaseDB {
    name: string;
    phone: string;
  }
  interface UpdateLogDB extends BaseDB {
    id: number;
    message: string;
    messageType: number;
    entityType: number;

    userInput?: undefined;
  }

  interface RoomDB extends BaseDB {
    roomName: string;
    listedItemss: ItemDB[];
  }
  interface ItemDB extends BaseDB {
    itemName: string;
    itemQuantity: number;
  }

  interface RsvTempStore<T> {
    values: string[];
    datas: T[];
  }

  namespace Reservation {
    interface Scope {
      pack: Pack;
    }

    interface Pack {
      [key: string]: ReservationDB[];
    }
  }

  type AccAuthorities = "ROLE_ADMIN" | "ROLE_USER";
  interface UserDB {
    id: number;
    activated: boolean;
    authorities: AccAuthorities[];
    createdDate: string;
    email: string;
    firstName: string;
    lastName: string;
    langKey: string;
    lastModifiedBy: string;
    lastModifiedDate: string;
    login: string;
    password: null;
  }

  type ReservationResource = ngService.IResource<ReservationDB>;
  type RoomResource = ngService.IResource<RoomDB>;
  type ItemResource = ngService.IResource<ItemDB>;
  type UserResource = ngService.IResource<UserDB>;
}

interface MultiFilter {
  (pageName: string): MultiFilterService;
}
interface MultiFilterService {
  getPageByKeywords(searchType: string, key: string | number): IServiceProvider;
  getPageSpecifyByUserAndKeywords(
    searchType: string,
    key: string
  ): IServiceProvider;
}

interface IServiceProvider {
  query(
    params: any,
    callback: (result: any, header: any) => void
  ): angular.IPromise<any>;
  query(callback: (result: any, header: any) => void): angular.IPromise<any>;
  get(): angular.IPromise<any>;
  update(result: any): angular.IPromise<any>;
  save(result: any): angular.IPromise<any>;
}

namespace ngController {
  namespace Carol {
    interface imageProp {
      id: number;
      src: string;
    }
  }

  interface IRootScope extends angular.IRootScopeService {
    isAuthenticated(): boolean;
    account?: Rv.UserDB;

    [propKey: string]: any;
  }

  interface IScopeExtended extends ng.IScope {
    $on(
      evt: "$locationChangeSuccess",
      listener: (
        this: this,
        evtObj: ng.IAngularEvent,
        newLocation: string,
        oldLocation: string
      ) => void
    ): this;
    $on(
      evt: string,
      listener: (evt: ng.IAngularEvent, ...args: any[]) => void
    ): this;

    [key: string]: any;
  }

  interface IBaseEntityScope<T = string> extends IScopeExtended {
    $filter: IFilterCtrl;

    pageName: T;
    page: number;
    predicate: string;
    reverse: boolean;
    filters: string[];
    filterValues?: string[];

    clear(): void;
    refresh(only?: boolean): void;
    loadAll(user?: boolean): void;
    loadAll(): void;
    loadPage(page: number): void;
  }

  interface RootAccount {
    ifDefined<T>(callback: (account: Rv.UserDB) => T): T;
    ifEmpty(callback: () => void): this;
  }

  type IEntityScope<T = {}, K = string> = IBaseEntityScope<K> & T;
}

namespace ngService {
  interface IResourceParams {
    page?: number;
    size?: number;
    sort: string[];
  }

  interface IPrincipal {
    authenticate(identity: Rv.UserDB): void;
    identity(force?: boolean): ng.IPromise<Rv.UserDB>;
    identityAccData(this: this, force?: boolean): any
    isAuthenticated(): boolean;
    isIdentityResolved(): boolean;
    hasAuthority(this: this, authority: string): ng.IPromise<boolean>;
    hasAnyAuthority(this: this, authorities: string[]): boolean;
    hasOnlyAuthority?(this: this, authorities): ng.IPromise<boolean>;
  }

  interface IAuth {
    login(): angular.IPromise<any>;
    logout(): void;
    authorize(): void;
    createAccount(): void;
    updateAccount(): void;
    activateAccount(): void;
    changePassword(): void;
    resetPasswordInit(): void;
    resetPasswordFinish(): void;
  }

  type IMultiFilter = <T extends keyof IMultiFilterKeys = string>(
    pageName: T
  ) => IMultiFilterFn<IMultiFilterKeys[T]>;
  interface IMultiFilterFn<T> {
    getPageByKeywords(
      options: MultiFilterOptionsByKeywords,
      callback: MultiFilterResource<T>
    ): void;
    // getPageSpecifyByUserAndKeywords(searchType: string, key: string): angular.IPromise<angular.resource.IResourceClass<angular.resource.IResource<T>>>
    getPageByStatus(
      code: number,
      userAsQuey: boolean = false,
      callback: (
        $resource: angular.resource.IResourceClass<
          angular.resource.IResource<T>
        >
      ) => void
    ): void;

    [key: string]: any;
  }

  interface IMultiFilterKeys {
    Reservation: Rv.ReservationDB;
    Item: Rv.ItemDB;
    Room: Rv.RoomDB;
  }

  interface MultiFilterOptionsByKeywords {
    searchType: string;
    key: string;
    user?: boolean;
  }
  type MultiFilterResource<T> = (
    $resource: angular.resource.IResourceClass<angular.resource.IResource<T>>
  ) => void;

  type IResource<T> = IResourceClass<T>

  interface IResourceClass<T> {
    new(dataOrParams?: any): T & IResource<T>;
    get: IResourceMethod<T>;

    query: IResourceArrayMethod<T>;

    save: IResourceMethod<T>;

    remove: IResourceMethod<T>;

    delete: IResourceMethod<T>;
    update: IResourceMethod<T>;

    [key: string]: any;
  }

  interface IResourceMethod<T> {
    (): T;
    (params: Object): T;
    (success: ResourceFnSucces<T>, error?: ResourceFnError<T>): T;
    (params: Object, success: ResourceFnSucces<T>, error?: ResourceFnError<T>): T;
    (params: Object, data: Object, success?: ResourceFnSucces<T>, error?: ResourceFnError<T>): T;
  }
  interface IResourceArrayMethod<T> {
    (): ng.resource.IResourceArray<T>;
    (params: Object): ng.resource.IResourceArray<T>;
    (success: Function, error?: Function): ng.resource.IResourceArray<T>;
    (params: Object, success: Function, error?: Function): ng.resource.IResourceArray<T>;
    (params: Object, data: Object, success?: Function, error?: Function): ng.resource.IResourceArray<T>;
  }
  type ResourceFnSucces<T> = (result: T, header: any) => any;
  type ResourceFnError<T> = (reaseon: any) => any;
  type ResourceParam<T> = T & { [key: string]: any }
}

interface Pageable {
  size?: number;
  sort?: string;
  page?: number;
  offset?: number;
}

interface Object {
  __proto__: any;
}