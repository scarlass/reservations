/// <reference path="../../Main.d.ts" />

interface PageByKeywords {
  searchType: string;
  key: string;
  user?: boolean;
}

ngApp
  .factory('MultiFilter', function ($http,
    $resource: angular.resource.IResourceService,
    Principal: ngService.IPrincipal) {
    return function <T extends keyof ngService.IMultiFilterKeys>(
      pageName: T
    ): ngService.IMultiFilterFn<ngService.IMultiFilterKeys[T]> {
      const page = pageName.toLowerCase().replace(' ', '');
      const url = `api/${page}s/search`.toLowerCase();

      return {
        getPageByKeywords(options: PageByKeywords, callback: ($resource: angular.resource.IResourceClass<angular.resource.IResource<any>>) => void) {
          function toSend(user: string = null) {
            return {
              searchType: options.searchType,
              userLogin: user,
              key: options.key
            }
          }

          if (options.user) {
            Principal.identity().then(res => {
              callback($resource(url, toSend(res.login), {}))
            });
          }
          else {
            callback($resource(url, toSend(), {}));
          }
        },

        getNotifiedData() {
          return Principal.identity().then(res => {
            return $resource('api/reservations/search', {
              login: res.login,
              notif: true
            }, {})
          });
        },

        getPageByStatus(code: number, userAsQuey: boolean = false, callback: ($resource: angular.resource.IResourceClass<angular.resource.IResource<any>>) => void) {
          function toSend(userLogin: string = null) {
            return {
              searchType: 'status',
              userLogin: userLogin,
              status: code
            }
          }

          if (userAsQuey)
            Principal.identity().then(res => {
              callback($resource(url, toSend(res.login), {}))
            });
          else callback($resource(url, toSend(), {}));
        }
      }
    }
  });

