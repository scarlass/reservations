"use strict";
ngApp
    .factory('MultiFilter', function ($http, $resource, Principal) {
    return function (pageName) {
        const page = pageName.toLowerCase().replace(' ', '');
        const url = `api/${page}s/search`.toLowerCase();
        return {
            getPageByKeywords(options, callback) {
                function toSend(user = null) {
                    return {
                        searchType: options.searchType,
                        userLogin: user,
                        key: options.key
                    };
                }
                if (options.user) {
                    Principal.identity().then(res => {
                        callback($resource(url, toSend(res.login), {}));
                    });
                }
                else {
                    callback($resource(url, toSend(), {}));
                }
            },
            getNotifiedData() {
                return Principal.identity().then(res => {
                    return $resource('api/reservations/search', {
                        login: res.login,
                        notif: true
                    }, {});
                });
            },
            getPageByStatus(code, userAsQuey = false, callback) {
                function toSend(userLogin = null) {
                    return {
                        searchType: 'status',
                        userLogin: userLogin,
                        status: code
                    };
                }
                if (userAsQuey)
                    Principal.identity().then(res => {
                        callback($resource(url, toSend(res.login), {}));
                    });
                else
                    callback($resource(url, toSend(), {}));
            }
        };
    };
});
