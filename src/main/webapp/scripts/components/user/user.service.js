'use strict';

angular.module('reservationsApp')
  .factory('User', function ($resource, DateUtils) {
    return $resource('api/users/:login', {}, {
      'query': { method: 'GET', isArray: true },
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
          if (data.lastModifiedDate !== null || data.lastModifiedDate !== undefined)
            data.lastModifiedDate = DateUtils.convertDateTimeFromServer(data.lastModifiedDate)

          return data;
        }
      },
      'save': { method: 'POST' },
      'update': { method: 'PUT' },
      'delete': { method: 'DELETE' }
    });
  });
