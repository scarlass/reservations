/// <reference path="../../Main.d.ts" />
'use strict';

angular.module('reservationsApp')
  .controller('NavbarController', function (
    $scope,
    $rootScope,
    $location,
    $state,
    Auth,
    /** @type {ngService.IPrincipal} */
    Principal,
    ENV
  ) {
    $scope.isAuthenticated = Principal.isAuthenticated;
    $scope.$state = $state;
    $scope.inProduction = ENV === 'prod';

    $scope.logout = function () {
      Auth.logout();
      $state.go('login');
    };

    Object.defineProperties($scope, {
      isAdmin: {
        get() {
          var arr = $rootScope.account.authorities;
          return arr.indexOf("ROLE_ADMIN") !== -1;
        }
      }
    })
  });
