/// <reference path="../../Main.d.ts" />

ngApp
  .directive('mFab', function () {
    return {
      restrict: 'A',
      link: function ($scope, $el, $atr) {
        function parseBoolean(val: string) {
          switch (val) {
            case 'true': {
              return true
            }
            case 'false':
              return false;
            default: {
              if (typeof val === 'undefined') return;
              let isnan = typeof +val !== "number";

              if (isnan) {
                let num = +val;
                return num === 0 ? false : true;
              }
              else {
                let prop = $scope[val];
                if (typeof prop === 'boolean') {
                  return prop;
                }
                else return !!prop

              }
            }
          }
        }

        let key = $atr.mFab as string;
        if (key === '') key = '$fab';

        let opt: Partial<M.FloatingActionButtonOptions> = {
          direction: $atr.mFabDirection || 'top',
          hoverEnabled: parseBoolean($atr.mFabHover) || true,
          toolbarEnabled: parseBoolean($atr.mFabToolbar) || false
        }
        let inst = $scope[key] = M.FloatingActionButton.init($el[0], opt);
      }
    }
  })