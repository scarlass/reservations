 'use strict';

angular.module('reservationsApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-reservationsApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-reservationsApp-params')});
                }
                return response;
            }
        };
    });
