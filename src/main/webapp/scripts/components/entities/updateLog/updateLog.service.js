'use strict';

angular.module('reservationsApp')
    .factory('UpdateLog', function ($resource, DateUtils) {
        return $resource('api/updateLogs/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.timeInput = DateUtils.convertDateTimeFromServer(data.timeInput);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
