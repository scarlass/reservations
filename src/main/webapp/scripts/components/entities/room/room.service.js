'use strict';

angular.module('reservationsApp')
  .factory('Room', function ($resource, DateUtils) {
    return $resource('api/rooms/:id', {}, {
      'query': { method: 'GET', isArray: true },
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.timeInput = DateUtils.convertDateTimeFromServer(data.timeInput);
          return data;
        }
      },
      'update': { method: 'PUT' }
    });
  })
  .factory('RoomService', function ($http, $resource) {
    var onGetSuccess = function (resp) {
      return resp.data;
    };

    var onGetError = function (resp) {
      return resp.data;
    };

    return {
      //PAGE
      getPageByKeyword: function (kolomPencarian, kataPencarian) {
        return $resource('api/rooms/getPageByKeyword', {
          kolomPencarian: kolomPencarian,
          kataPencarian: kataPencarian
        }, {});
      }
    }
  });