'use strict';

angular.module('reservationsApp')
    .factory('Customer', function ($resource, DateUtils) {
        return $resource('api/customers/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.timeInput = DateUtils.convertDateTimeFromServer(data.timeInput);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
