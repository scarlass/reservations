'use strict';

angular.module('reservationsApp')
    .factory('Reservation', function ($resource, DateUtils) {
        return $resource('api/reservations/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.usageTimeStart = DateUtils.convertDateTimeFromServer(data.usageTimeStart);
                    data.usageTimeEnd = DateUtils.convertDateTimeFromServer(data.usageTimeEnd);
                    data.timeInput = DateUtils.convertDateTimeFromServer(data.timeInput);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
