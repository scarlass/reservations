ngApp
  .factory('Reservation', function ($resource: angular.resource.IResourceService, DateUtils) {
    return $resource('api/reservations/:id', {}, {
      'query': { method: 'GET', isArray: true },
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.usageTimeStart = DateUtils.convertDateTimeFromServer(data.usageTimeStart);
          data.usageTimeEnd = DateUtils.convertDateTimeFromServer(data.usageTimeEnd);
          data.timeInput = DateUtils.convertDateTimeFromServer(data.timeInput);
          return data;
        }
      },
      'update': { method: 'PUT' }
    });
  })
  .factory('Customer', function ($resource, DateUtils) {
    return $resource('api/customers/:id', {}, {
      'query': { method: 'GET', isArray: true },
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.timeInput = DateUtils.convertDateTimeFromServer(data.timeInput);
          return data;
        }
      },
      'update': { method: 'PUT' }
    });
  })
  .factory('Room', function ($resource, DateUtils) {
    return $resource('api/rooms/:id', {}, {
      'query': { method: 'GET', isArray: true },
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.timeInput = DateUtils.convertDateTimeFromServer(data.timeInput);
          return data;
        }
      },
      'update': { method: 'PUT' }
    });
  })
  .factory('UpdateLog', function ($resource, DateUtils) {
    return $resource('api/updateLogs/:id', {}, {
      'query': { method: 'GET', isArray: true },
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.timeInput = DateUtils.convertDateTimeFromServer(data.timeInput);
          return data;
        }
      },
      'update': { method: 'PUT' }
    });
  })
  .factory('Item', function ($resource, DateUtils) {

    return $resource('api/items/:id', {}, {
      'query': { method: 'GET', isArray: true },
      'get': {
        method: 'GET',
        transformResponse: function (data) {
          data = angular.fromJson(data);
          data.timeInput = DateUtils.convertDateTimeFromServer(data.timeInput);
          return data;
        }
      },
      'update': { method: 'PUT' }
    });
  });


  // .factory('RoomService', function ($http, $resource) {
  //   var onGetSuccess = function (resp) {
  //     return resp.data;
  //   };

  //   var onGetError = function (resp) {
  //     return resp.data;
  //   };

  //   return {
  //     //PAGE
  //     getPageByKeyword: function (kolomPencarian, kataPencarian) {
  //       return $resource('api/rooms/getPageByKeyword', {
  //         kolomPencarian: kolomPencarian,
  //         kataPencarian: kataPencarian
  //       }, {});
  //     }
  //   }
  // })