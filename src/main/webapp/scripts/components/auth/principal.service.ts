/// <reference path="../../Main.d.ts" />
"use strict";

angular
  .module("reservationsApp")
  .factory(
    "Principal",
    function Principal(
      $q: angular.IQService,
      Account: ngService.IResourceClass<any>,
      $rootScope: angular.IRootScopeService,
      User: ngService.IResourceClass<Rv.UserDB>
    ) {
      var _identity: Rv.UserDB,
        _authenticated = false;

      const Principal: ngService.IPrincipal = {
        isIdentityResolved() {
          return angular.isDefined(_identity);
        },
        isAuthenticated() {
          return _authenticated;
        },
        hasOnlyAuthority(authority) {
          return this.identity().then(
            function (_id: Rv.UserDB) {
              let roles = _id.authorities;
              return roles.length === 1 && roles[0] === authority;
            },
            function (err) {
              return false;
            }
          );
        },
        hasAuthority(authority: Rv.AccAuthorities) {
          if (!_authenticated) {
            return $q.when(false);
          }

          return this.identity().then(
            function (_id) {
              return (
                _id.authorities && _id.authorities.indexOf(authority) !== -1
              );
            },
            function (err) {
              return false;
            }
          );
        },
        hasAnyAuthority(authorities) {
          if (!_authenticated || !_identity || !_identity.authorities) {
            return false;
          }

          for (var i = 0; i < authorities.length; i++) {
            // @ts-ignore
            if (_identity.authorities.indexOf(authorities[i]) !== -1) {
              return true;
            }
          }

          return false;
        },
        authenticate(identity) {
          _identity = identity;
          _authenticated = identity !== null;
        },
        identity(force?: boolean) {
          var deferred = $q.defer<Rv.UserDB>();
          if (force === true) {
            _identity = undefined;
          }
          // check and see if we have retrieved the identity data from the server.
          // if we have, reuse it by immediately resolving
          if (angular.isDefined(_identity)) {
            deferred.resolve(_identity);
            return deferred.promise;
          }
          // retrieve the identity data from the server, update the identity object, and then resolve.
          Account.get()
            .$promise.then(function (account) {
              _identity = account.data;
              _authenticated = true;
              deferred.resolve(_identity);
            })
            .catch(function () {
              _identity = null;
              _authenticated = false;
              deferred.resolve(_identity);
            });
          return deferred.promise;
        },
        identityAccData(force) {
          let deferred = $q.defer<Rv.UserDB>();
          if (isDefined(_identity)) {
            User.get({ login: _identity.login }, function (res) {
              deferred.resolve(res);
            });
            return deferred.promise;
          }

          this.identity(force).then(res => {
            if (isDefined(res)) {
              User.get({ login: res.login }, function (res) {
                deferred.resolve(res);
              });
            }
            else deferred.resolve(res);

          }).catch(err => {
            deferred.resolve(err);
          });
          return deferred.promise
        }
      };

      return Principal;
    }
  );

// class UserDB implements Rv.UserDB {
//   constructor(data: Rv.UserDB) {
//     for (let k in data) {
//       this[k] = data[k];
//     }
//   }

//   activated: boolean;
//   authorities: Rv.AccAuthorities[];
//   createdDate: string;
//   email: string;
//   firstName: string;
//   lastName: string;
//   lastModifiedBy: string;
//   lastModifiedDate: string;
//   id: number;
//   langKey: string;
//   login: string;
//   password: null;
// }