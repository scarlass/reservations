"use strict";
angular
    .module("reservationsApp")
    .factory("Principal", function Principal($q, Account, $rootScope, User) {
    var _identity, _authenticated = false;
    const Principal = {
        isIdentityResolved() {
            return angular.isDefined(_identity);
        },
        isAuthenticated() {
            return _authenticated;
        },
        hasOnlyAuthority(authority) {
            return this.identity().then(function (_id) {
                let roles = _id.authorities;
                return roles.length === 1 && roles[0] === authority;
            }, function (err) {
                return false;
            });
        },
        hasAuthority(authority) {
            if (!_authenticated) {
                return $q.when(false);
            }
            return this.identity().then(function (_id) {
                return (_id.authorities && _id.authorities.indexOf(authority) !== -1);
            }, function (err) {
                return false;
            });
        },
        hasAnyAuthority(authorities) {
            if (!_authenticated || !_identity || !_identity.authorities) {
                return false;
            }
            for (var i = 0; i < authorities.length; i++) {
                if (_identity.authorities.indexOf(authorities[i]) !== -1) {
                    return true;
                }
            }
            return false;
        },
        authenticate(identity) {
            _identity = identity;
            _authenticated = identity !== null;
        },
        identity(force) {
            var deferred = $q.defer();
            if (force === true) {
                _identity = undefined;
            }
            if (angular.isDefined(_identity)) {
                deferred.resolve(_identity);
                return deferred.promise;
            }
            Account.get()
                .$promise.then(function (account) {
                _identity = account.data;
                _authenticated = true;
                deferred.resolve(_identity);
            })
                .catch(function () {
                _identity = null;
                _authenticated = false;
                deferred.resolve(_identity);
            });
            return deferred.promise;
        },
        identityAccData(force) {
            let deferred = $q.defer();
            if (isDefined(_identity)) {
                User.get({ login: _identity.login }, function (res) {
                    deferred.resolve(res);
                });
                return deferred.promise;
            }
            this.identity(force).then(res => {
                if (isDefined(res)) {
                    User.get({ login: res.login }, function (res) {
                        deferred.resolve(res);
                    });
                }
                else
                    deferred.resolve(res);
            }).catch(err => {
                deferred.resolve(err);
            });
            return deferred.promise;
        }
    };
    return Principal;
});
