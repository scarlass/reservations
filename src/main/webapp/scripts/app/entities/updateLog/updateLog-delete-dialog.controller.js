'use strict';

angular.module('reservationsApp')
	.controller('UpdateLogDeleteController', function($scope, $uibModalInstance, entity, UpdateLog) {

        $scope.updateLog = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            UpdateLog.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
