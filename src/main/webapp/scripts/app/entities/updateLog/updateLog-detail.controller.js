'use strict';

angular.module('reservationsApp')
    .controller('UpdateLogDetailController', function ($scope, $rootScope, $stateParams, DataUtils, entity, UpdateLog, Reservation, Room, Item) {
        $scope.updateLog = entity;
        $scope.load = function (id) {
            UpdateLog.get({id: id}, function(result) {
                $scope.updateLog = result;
            });
        };
        var unsubscribe = $rootScope.$on('reservationsApp:updateLogUpdate', function(event, result) {
            $scope.updateLog = result;
        });
        $scope.$on('$destroy', unsubscribe);

        $scope.byteSize = DataUtils.byteSize;
    });
