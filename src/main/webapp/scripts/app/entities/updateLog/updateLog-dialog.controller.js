'use strict';

angular.module('reservationsApp').controller('UpdateLogDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'UpdateLog', 'Reservation', 'Room', 'Item',
        function($scope, $stateParams, $uibModalInstance, DataUtils, entity, UpdateLog, Reservation, Room, Item) {

        $scope.updateLog = entity;
        $scope.reservations = Reservation.query();
        $scope.rooms = Room.query();
        $scope.items = Item.query();
        $scope.load = function(id) {
            UpdateLog.get({id : id}, function(result) {
                $scope.updateLog = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('reservationsApp:updateLogUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.updateLog.id != null) {
                UpdateLog.update($scope.updateLog, onSaveSuccess, onSaveError);
            } else {
                UpdateLog.save($scope.updateLog, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.abbreviate = DataUtils.abbreviate;

        $scope.byteSize = DataUtils.byteSize;
        $scope.datePickerForTimeInput = {};

        $scope.datePickerForTimeInput.status = {
            opened: false
        };

        $scope.datePickerForTimeInputOpen = function($event) {
            $scope.datePickerForTimeInput.status.opened = true;
        };
}]);
