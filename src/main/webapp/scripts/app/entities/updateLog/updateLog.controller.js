'use strict';
angular.module('reservationsApp')
    .controller('UpdateLogController', function ($scope, $state, DataUtils, UpdateLog, ParseLinks) {
    const { EntityType: et, MessageType: mt } = Rv;
    $scope.updateLogs = [];
    $scope.predicate = 'id';
    $scope.reverse = true;
    $scope.page = 1;
    $scope.loadAll = function () {
        UpdateLog.query({ page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id'] }, function (result, headers) {
            $scope.links = ParseLinks.parse(headers('link'));
            $scope.totalItems = headers('X-Total-Count');
            $scope.updateLogs = result;
            console.log(result);
        });
    };
    $scope.loadPage = function (page) {
        $scope.page = page;
        $scope.loadAll();
    };
    $scope.loadAll();
    $scope.refresh = function () {
        $scope.loadAll();
        $scope.clear();
    };
    $scope.clear = function () {
        $scope.updateLog = {
            message: null,
            messageType: null,
            additionalMessage: null,
            entityType: null,
            timeInput: null,
            id: null
        };
    };
    $scope.abbreviate = DataUtils.abbreviate;
    $scope.byteSize = DataUtils.byteSize;
    $scope.entityLink = "";
    $scope.setEntityType = function (id) {
        return et[id];
    };
    $scope.setMessageType = function (id) {
        const res = mt[id];
        return res[0].toUpperCase() + res.slice(1);
    };
    $scope.setColor = function (id) {
        return {
            color: id === mt.CREATE ? 'green' : id === mt.DELETE ? 'red' : id === mt.UPDATED ? '#ff7728' : 'unset',
            fontWeight: '582'
        };
    };
    $scope.onAllEntitysNull = function (data) {
        return data.itemId === null && data.reservationId === null && data.roomId === null;
    };
    $scope.getPartOfMessage = function (data) {
        return /(\d+)/.exec(data.message)[0];
    };
});
