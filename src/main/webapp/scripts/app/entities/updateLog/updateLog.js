'use strict';

angular.module('reservationsApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('updateLog', {
                parent: 'entity',
                url: '/updateLogs',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'reservationsApp.updateLog.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/updateLog/updateLogs.html',
                        controller: 'UpdateLogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('updateLog');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('updateLog.detail', {
                parent: 'entity',
                url: '/updateLog/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'reservationsApp.updateLog.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/updateLog/updateLog-detail.html',
                        controller: 'UpdateLogDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('updateLog');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'UpdateLog', function($stateParams, UpdateLog) {
                        return UpdateLog.get({id : $stateParams.id});
                    }]
                }
            })
            .state('updateLog.new', {
                parent: 'updateLog',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/updateLog/updateLog-dialog.html',
                        controller: 'UpdateLogDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    message: null,
                                    messageType: null,
                                    additionalMessage: null,
                                    entityType: null,
                                    timeInput: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('updateLog', null, { reload: true });
                    }, function() {
                        $state.go('updateLog');
                    })
                }]
            })
            .state('updateLog.edit', {
                parent: 'updateLog',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/updateLog/updateLog-dialog.html',
                        controller: 'UpdateLogDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['UpdateLog', function(UpdateLog) {
                                return UpdateLog.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('updateLog', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('updateLog.delete', {
                parent: 'updateLog',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/updateLog/updateLog-delete-dialog.html',
                        controller: 'UpdateLogDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['UpdateLog', function(UpdateLog) {
                                return UpdateLog.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('updateLog', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
