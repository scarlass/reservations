"use strict";

angular.module("reservationsApp")
  .controller(
    "ItemController",
    function ($scope: ngController.IEntityScope<ItemController, 'Item'>, $rootScope, DataUtils, Item: Rv.ItemResource, ParseLinks, MultiFilter: ngService.IMultiFilter) {
      const s = $scope;
      s.pageName = "Item";
      s.items = [];
      s.predicate = "id";
      s.reverse = true;
      s.page = 1;
      s.loadAll = function () {
        Item.query({
          page: $scope.page - 1,
          size: 20,
          sort: [
            $scope.predicate + "," + ($scope.reverse ? "asc" : "desc"),
            "id",
          ],
        }, function (result, headers) {
          $scope.links = ParseLinks.parse(headers("link"));
          $scope.totalItems = headers("X-Total-Count");
          $scope.items = result;
        });

      };

      $scope.loadPage = function (page) {
        $scope.page = page;
        $scope.loadAll();
      };
      $scope.loadAll();

      $scope.refresh = function () {
        $scope.loadAll();
        $scope.clear();
      };

      $scope.clear = function () {
        $scope.item = {
          itemName: null,
          itemQuantity: null,
          itemDetails: null,
          userInput: null,
          timeInput: null,
          id: null,
        };
      };

      $scope.abbreviate = DataUtils.abbreviate;
      $scope.byteSize = DataUtils.byteSize;

      s.filters = ["ItemName"];

      const Flt = MultiFilter(s.pageName);
      s.$on(`${s.pageName}:Filter`, function (evt, evtObj) {
        let st = evtObj.searchType as string;
        let key = evtObj.key as string;
        console.log(st, key);

        Flt.getPageByKeywords(evtObj, function ($r) {
          $r.query(function (result) { s.items = result });
        });
      });

      s.$emit("FloatText", s.pageName);
    }
  );

interface ItemController {
  items: Rv.ItemDB[];
}