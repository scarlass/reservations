/// <reference path="../../../Main.d.ts" />
'use strict';



angular.module('reservationsApp').controller('ItemDialogController',
  ['$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Item', '$rootScope', 'Principal',
    function ($scope: ItemDialogController, $stateParams, $uibModalInstance, DataUtils, entity, Item: Rv.ItemResource, $rootScope: ngController.IRootScope, Principal: ngService.IPrincipal) {
      const s = $scope;
      $scope.item = entity;

      $scope.load = function (id: number) {
        Item.get({ id: id }, function (result) {
          $scope.item = result;
        });
      };

      var onSaveSuccess = function (result) {
        $scope.$emit('reservationsApp:itemUpdate', result);
        $uibModalInstance.close(result);
        $scope.isSaving = false;
      };

      var onSaveError = function (result) {
        $scope.isSaving = false;
      };

      $scope.save = function () {
        Principal.identityAccData().then(res => {
          if (!isDefined(this.item.userInput))
            this.item.userInput = res.login;

          $scope.isSaving = true;
          if (this.item.id != null) {
            Item.update(this.item, onSaveSuccess, onSaveError);
          } else {
            Item.save(this.item, onSaveSuccess, onSaveError);
          }
        })
      };

      $scope.clear = function () {
        $uibModalInstance.dismiss('cancel');
      };

      $scope.abbreviate = DataUtils.abbreviate;

      $scope.byteSize = DataUtils.byteSize;
      $scope.datePickerForTimeInput = {};

      $scope.datePickerForTimeInput.status = {
        opened: false
      };

      $scope.datePickerForTimeInputOpen = function ($event) {
        $scope.datePickerForTimeInput.status.opened = true;
      };
    }]);

interface ItemDialogController extends ngController.IScopeExtended {
  item: Rv.ItemDB;
  save(this: this): void;
  load(this: this, id: number): void
}