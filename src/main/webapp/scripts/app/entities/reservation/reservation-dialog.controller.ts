/// <reference path="../../../Main.d.ts" />
'use strict';

interface ReservationDialogController {
  reservation: Rv.ReservationDB;
}

(function (this: angular.IModule) {
  function ReservationDialogController(
    $scope: ngController.IEntityScope<ReservationDialogController>,
    $stateParams,
    $uibModalInstance,
    DataUtils,
    entity,
    Reservation: ngService.IResource<Rv.ReservationDB>,
    Room: ngService.IResource<Rv.RoomDB>,
    Principal: ngService.IPrincipal,
    $rootScope: ngController.IRootScope
  ) {
  
    $scope.statusValue = Rv.Status;
    $scope.reservation = entity;
    $scope.rooms = Room.query();
  
    $scope.load = function (id: number) {
      Reservation.get({ id: id }, function (result: Rv.ReservationDB) {
        $scope.reservation = result;
      });
    };
  
    var onSaveSuccess = function (result) {
      $scope.$emit('reservationsApp:reservationUpdate', result);
      $uibModalInstance.close(result);
      $scope.isSaving = false;
    };
  
    var onSaveError = function (result) {
      $scope.isSaving = false;
    };
  
    $scope.save = function () {
      console.log('Saving Reservations !')
      const { account } = $rootScope;
      const { reservation } = $scope
  
      Principal.identityAccData().then(results => {
        reservation.userLogin = reservation.userLogin === null || reservation.userId === undefined ? results.login : reservation.userLogin;
        reservation.userId = reservation.userId === null || reservation.userId === undefined ? results.id : reservation.userId;
  
        if (reservation.roomUpdateInfo !== null) {
          $scope.reservation.roomUpdateInfo = null;
        }
  
        $scope.isSaving = true;
        console.log(reservation)
        if ($scope.reservation.id != null) {
          Reservation.update($scope.reservation, onSaveSuccess, onSaveError);
        } else {
          Reservation.save($scope.reservation, onSaveSuccess, onSaveError);
        }
      })
    };
  
    $scope.clear = function () {
      $uibModalInstance.dismiss('cancel');
    };
  
    $scope.abbreviate = DataUtils.abbreviate;
  
    $scope.byteSize = DataUtils.byteSize;
  
    $scope.datePickerForUsageTimeStart = {};
    $scope.datePickerForUsageTimeStart.status = {
      opened: false
    };
    $scope.datePickerForUsageTimeStartOpen = function ($event) {
      $scope.datePickerForUsageTimeStart.status.opened = true;
    };
  
    $scope.datePickerForUsageTimeEnd = {};
    $scope.datePickerForUsageTimeEnd.status = {
      opened: false
    };
    $scope.datePickerForUsageTimeEndOpen = function ($event) {
      $scope.datePickerForUsageTimeEnd.status.opened = true;
    };
  
    $scope.datePickerForTimeInput = {};
    $scope.datePickerForTimeInput.status = {
      opened: false
    };
    $scope.datePickerForTimeInputOpen = function ($event) {
      $scope.datePickerForTimeInput.status.opened = true;
    };
  
  }
  
  this.controller('ReservationDialogController', ReservationDialogController);
}).call(ngApp);
