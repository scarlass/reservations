'use strict';

angular.module('reservationsApp')
    .controller('ReservationDetailController', function ($scope, $rootScope, $stateParams, DataUtils, entity, Reservation, Room) {
        $scope.reservation = entity;
        $scope.load = function (id) {
            Reservation.get({id: id}, function(result) {
                $scope.reservation = result;
            });
        };
        var unsubscribe = $rootScope.$on('reservationsApp:reservationUpdate', function(event, result) {
            $scope.reservation = result;
        });
        $scope.$on('$destroy', unsubscribe);

        $scope.byteSize = DataUtils.byteSize;
    });
