"use strict";
(function () {
    function ReservationController($scope, $state, DataUtils, Reservation, ParseLinks, $rootScope, Principal, MultiFilter) {
        const rt = $rootScope;
        const s = $scope;
        s.pageName = "Reservation";
        s.statusValue = Rv.Status;
        s.setStatus = function (value) {
            if (value === undefined || value === null) {
                return "";
            }
            const val = $scope.statusValue[value];
            if (val === undefined)
                return "";
            return UppercaseFirstLetter(val.text);
        };
        s.delBtnState = function (reservation) {
            let user = rt.account;
            return (user.login === reservation.userLogin ||
                user.authorities.includes("ROLE_ADMIN"));
        };
        s.account = $rootScope.account;
        s.reservations = [];
        s.predicate = "id";
        s.reverse = true;
        s.page = 1;
        s.loadAll = function (userOnly) {
            Principal.identity().then((res) => {
                let isAdmin = res.authorities.includes("ROLE_ADMIN");
                let length = res.authorities.length;
                let isUserOnly = length === 1 && res.authorities.includes("ROLE_USER");
                if (userOnly) {
                    const Flt = MultiFilter(s.pageName);
                    Flt.getPageByKeywords({
                        searchType: "login",
                        key: res.login,
                    }, function ($r) {
                        $r.query({
                            page: $scope.page - 1,
                            size: 20,
                            sort: [
                                $scope.predicate + "," + ($scope.reverse ? "asc" : "desc"),
                                "id",
                            ],
                        }, function (result, headers) {
                            $scope.links = ParseLinks.parse(headers("link"));
                            $scope.totalItems = headers("X-Total-Count");
                            $scope.reservations = result;
                        });
                    });
                }
                else
                    Reservation.query({
                        page: $scope.page - 1,
                        size: 20,
                        sort: [
                            $scope.predicate + "," + ($scope.reverse ? "asc" : "desc"),
                            "id",
                        ],
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers("link"));
                        $scope.totalItems = headers("X-Total-Count");
                        $scope.reservations = result;
                    });
            });
        };
        s.loadPage = function (page) {
            $scope.page = page;
            $scope.loadAll();
        };
        s.loadAll();
        s.refresh = function (userOnly) {
            if (userOnly)
                $scope.loadAll(userOnly);
            else
                $scope.loadAll();
            $scope.clear();
        };
        s.clear = function () {
            $scope.reservation = {
                userId: null,
                userLogin: null,
                usageTimeStart: null,
                usageTimeEnd: null,
                details: null,
                timeInput: null,
                status: null,
                id: null,
            };
        };
        s.abbreviate = DataUtils.abbreviate;
        s.byteSize = DataUtils.byteSize;
        s.filters = ["Room", "User", "Status"];
        s.only = false;
        s.$on("Reservation:UserOnly", function (evt, only) {
            s.refresh(only);
            s.only = only;
        });
        s.$on(`${s.pageName}:Filter`, function (evt, opt) {
            let type = opt.searchType.toLowerCase();
            if (type === "user")
                type = "login";
            const Flt = MultiFilter(s.pageName);
            if (type === "status") {
                let code = getStatusCode(opt.key);
                log(code);
                Flt.getPageByStatus(code, s.only, function ($r) {
                    $r.query({
                        page: $scope.page - 1,
                        size: 20,
                        sort: [
                            $scope.predicate + "," + ($scope.reverse ? "asc" : "desc"),
                            "id",
                        ],
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers("link"));
                        $scope.totalItems = headers("X-Total-Count");
                        $scope.reservations = result;
                    });
                });
            }
            else {
                Flt.getPageByKeywords({
                    searchType: type,
                    key: opt.key,
                    user: s.only,
                }, function ($r) {
                    $r.query({
                        page: $scope.page - 1,
                        size: 20,
                        sort: [
                            $scope.predicate + "," + ($scope.reverse ? "asc" : "desc"),
                            "id",
                        ],
                    }, function (result, headers) {
                        $scope.links = ParseLinks.parse(headers("link"));
                        $scope.totalItems = headers("X-Total-Count");
                        $scope.reservations = result;
                    });
                });
            }
        });
        function getStatusCode(code) {
            let isNum = /^\d+/.test(code);
            if (isNum) {
                let num = +code;
                if (num <= 3 && num >= 0)
                    return num;
            }
            else {
                let keys = Object.keys(Rv.StatusCode);
                let find = keys.find((i) => new RegExp(`${code.toLowerCase()}`).test(i.toLowerCase()));
                if (find !== undefined) {
                    return Rv.StatusCode[find];
                }
            }
            return 0;
        }
    }
    ngApp.controller("ReservationController", ReservationController);
})();
