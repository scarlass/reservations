"use strict";

angular
  .module("reservationsApp")
  .controller("CustomerController", function (
    $scope,
    $rootScope,
    Customer,
    ParseLinks,
    /** @type {MultiFilter} */ MultiFilter
  ) {
    /** @type {angular.IScope} */
    const s = $scope;
    $scope.pageName = "Customer";
    $scope.customers = [];
    $scope.predicate = "id";
    $scope.reverse = true;
    $scope.page = 1;
    $scope.loadAll = function () {
      Customer.query(
        {
          page: $scope.page - 1,
          size: 20,
          sort: [
            $scope.predicate + "," + ($scope.reverse ? "asc" : "desc"),
            "id",
          ],
        },
        function (result, headers) {
          $scope.links = ParseLinks.parse(headers("link"));
          $scope.totalItems = headers("X-Total-Count");
          $scope.customers = result;
        }
      );

      Customer.query(function (result) {
        s.filterValues.CustomerName = result.map((i) => i.name);
        console.log(s);
      });
    };
    $scope.loadPage = function (page) {
      $scope.page = page;
      $scope.loadAll();
    };
    $scope.loadAll();

    $scope.refresh = function () {
      $scope.loadAll();
      $scope.clear();
    };

    $scope.clear = function () {
      $scope.customer = {
        name: null,
        phone: null,
        userInput: null,
        timeInput: null,
        id: null,
      };
    };

    $scope.filters = ["CustomerName"];
    $scope.filterValues = {
      CustomerName: [],
    };

    Object.defineProperties(this, {
      selected: {
        get: () => slcTemp,
        set: (value) => {
          slcTemp = value;
          this.selectedValues = this[value.toLowerCase()].values;
        },
      },
      startFilter: {
        value: () => {
          let slc = this[this.selected.toLowerCase()].datas;
          let val = this.inputValue;

          if (slc === undefined) return;

          let res;
          if (this.selected === "Customer") {
            res = slc.find((i) => i.name === val).id;
          } else if (this.selected === "Room") {
            res = slc.find((i) => i.roomName === val).id;
          }

          if (res === undefined) return;
          console.log(res);
          let d = ReservationService.getPageByKeywords(this.selected, res);
          d.query(function (res) {
            s.reservations = res;
          });
        },
      },
    });

    const FilterProvider = MultiFilter(s.pageName);
    s.$on(`${s.pageName}:Filter`, function (evt, evtObj) {
      /** @type {"CustomerName"} */
      let st = evtObj.searchType;
      /** @type {string} */
      let key = evtObj.key;
      console.log(st, key);

      FilterProvider.getPageByKeywords(st, key).query(function (result) {
        log(result);
        s.customers = result;
      });
    });

    s.$emit("FloatText", s.pageName);
  });
