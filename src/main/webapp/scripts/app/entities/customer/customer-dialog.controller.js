'use strict';

angular.module('reservationsApp').controller('CustomerDialogController',
  ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Customer',
    function ($scope, $stateParams, $uibModalInstance, entity, Customer) {

      $scope.customer = entity;
      $scope.load = function (id) {
        Customer.get({ id: id }, function (result) {
          $scope.customer = result;
        });
      };

      var onSaveSuccess = function (result) {
        $scope.$emit('reservationsApp:customerUpdate', result);
        $uibModalInstance.close(result);
        $scope.isSaving = false;
      };

      var onSaveError = function (result) {
        $scope.isSaving = false;
      };

      $scope.save = function () {
        $scope.isSaving = true;
        /** @type {string} */
        const nm = $scope.customer.name;
        $scope.customer.name = nm[0].toUpperCase() + nm.slice(1);

        if ($scope.customer.id != null) {
          Customer.update($scope.customer, onSaveSuccess, onSaveError);
        } else {
          Customer.save($scope.customer, onSaveSuccess, onSaveError);
        }
      };

      $scope.clear = function () {
        $uibModalInstance.dismiss('cancel');
      };
      $scope.datePickerForTimeInput = {};

      $scope.datePickerForTimeInput.status = {
        opened: false
      };

      $scope.datePickerForTimeInputOpen = function ($event) {
        $scope.datePickerForTimeInput.status.opened = true;
      };
    }]);
