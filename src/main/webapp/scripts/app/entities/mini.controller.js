"use strict";
ngApp.controller("FilterController", function FilterController($scope, Principal) {
    const s = $scope;
    const par = $scope.$parent;
    s.currentPage = par.pageName;
    let onlyCurrentUser = false;
    Object.defineProperty(s, "onlyCurrentUser", {
        get() {
            return onlyCurrentUser;
        },
        set(val) {
            onlyCurrentUser = val;
            s.$emit("Reservation:UserOnly", val);
        },
    });
    s.key = "";
    s.filterValues = par.filterValues;
    s.refresh = par.refresh;
    let selected = s.filters ? s.filters[0] : undefined;
    Object.defineProperties(s, {
        selectedFilter: {
            get: () => selected,
            set: function (value) {
                selected = value;
            },
        },
    });
    s.setFilterValue = function (filter) {
        s.selectedFilter = filter;
    };
    s.convert = function (flt) {
        return flt.replace(/^(\w+)Name/, "Name");
    };
    s.startFilter = function () {
        s.$emit(`${s.currentPage}:Filter`, {
            searchType: selected.replace(" ", ""),
            key: s.key,
        });
    };
    Principal.identity().then((acc) => {
        let role = acc.authorities;
        let isOnlyuser = role.length === 1 && role[0] === "ROLE_USER";
        s.acc = acc;
        setTimeout(function () {
            s.onlyCurrentUser = s.showIfUser = isOnlyuser;
        }, 50);
        s.filters = (function (flt) {
            let curr = s.currentPage.toLowerCase() === "reservation";
            if (curr && !s.onlyCurrentUser) {
                if (!s.acc.authorities.includes("ROLE_ADMIN")) {
                    let idx = flt.indexOf("User");
                    flt.splice(idx, 1);
                }
            }
            return flt;
        })(par.filters);
    });
    s.setDropdown = function (val) {
        let isReservation = s.currentPage.toLowerCase() === "reservation";
        if (isReservation && val) {
            let index = s.filters.indexOf("User");
            if (index !== -1) {
                s.filters.splice(index, 1);
            }
        }
        else if (isReservation && !val) {
            if (!s.filters.includes("User")) {
                s.filters.push("User");
            }
        }
    };
    par.$filter = s;
    angular.element("#filter-input-field").on("keydown", function (evt) {
        if (evt.keyCode === 13) {
            s.startFilter();
        }
    });
});
ngApp.controller("FloatButtonCtrl", function FloatButtonCtrl($scope, $rootScope) {
    const s = $scope;
    const rt = $rootScope;
    s.placement = "top";
    s.currentPage = "";
    const predictedPath = /(items|customers|rooms|reservations)/;
    Object.defineProperties(s, {
        show: {
            get() {
                return predictedPath.test(s.currentPage);
            },
        },
        hrefLink: {
            get: () => `#/${s.currentPage}/new`,
        },
        icon: {
            get() {
                if (predictedPath.test(s.currentPage)) {
                    return true;
                }
                else if (s.currentPage === "home") {
                    if (rt.isAuthenticated && rt.isAuthenticated()) {
                        return true;
                    }
                    return false;
                }
                else
                    return false;
            },
        },
    });
    s.$on("$locationChangeSuccess", function (evt, newLoc, oldLoc) {
        log(newLoc);
        let isHome = /(\/|\\)#(\/|\\)$/.test(newLoc);
        if (isHome)
            s.currentPage = "home";
        else {
            let loc = /#(\/|\\)\S+$/.exec(newLoc);
            if (loc === null || loc === undefined)
                return;
            if (loc.length === 0)
                return;
            let n = loc[0].replace("#/", "");
            s.currentPage = n;
        }
    });
    s.openModalDialogue = function () {
        let link = angular.element("#open-new-modal");
    };
});
