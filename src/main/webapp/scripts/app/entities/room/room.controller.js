"use strict";

angular.module("reservationsApp").controller("RoomController", function (
  /**  */
  $scope,
  $rootScope,
  Room,
  ParseLinks,
  /** @type {MultiFilter} */ MultiFilter
) {
  /** @type {angular.IScope} */
  const s = $scope;

  s.pageName = "Room";
  const FilterProvider = MultiFilter(s.pageName);
  s.rooms = [];
  s.predicate = "id";
  s.reverse = true;
  s.page = 1;
  s.loadAll = function () {
    Room.query(
      {
        page: $scope.page - 1,
        size: 20,
        sort: [
          $scope.predicate + "," + ($scope.reverse ? "asc" : "desc"),
          "id",
        ],
      },
      function (result, headers) {
        $scope.links = ParseLinks.parse(headers("link"));
        $scope.totalItems = headers("X-Total-Count");
        $scope.rooms = result;
        log(result)
      }
    );
  };
  s.loadPage = function (page) {
    $scope.page = page;
    $scope.loadAll();
  };
  s.loadAll();

  s.refresh = function () {
    $scope.loadAll();
    $scope.clear();
  };

  s.clear = function () {
    $scope.room = {
      roomName: null,
      userInput: null,
      timeInput: null,
      id: null,
    };
  };

  s.filters = ["RoomName"];
  s.filterValues = {
    RoomName: [],
  };

  s.$on(`${s.pageName}:Filter`, function (evt, evtObject) {
    /** @type {"RoomName"} */
    let st = evtObject.searchType;
    /** @type {string} */
    let key = evtObject.key;
    console.log(st, key);

    FilterProvider.getPageByKeywords(st, key).query(function (result) {
      s.rooms = result;
    });
  });

  s.$emit("FloatText", s.pageName);

});
