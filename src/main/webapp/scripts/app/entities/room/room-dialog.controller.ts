/// <reference path="../../../Main.d.ts" />
'use strict';

angular.module('reservationsApp')
  .controller('RoomDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Room', 'Item', 'Principal',
      function (s: RoomDialogController, $stateParams, $uibModalInstance, entity: Rv.RoomDB, Room: Rv.RoomResource, Item: Rv.ItemResource, Principal: ngService.IPrincipal) {
        // console.log($stateParams)
        s.checkedItems = {};
        s.items = Item.query(function (result) {
          result.forEach(item => {
            s.checkedItems[item.id] = false;
          });
        });
        s.room = entity;
        setTimeout(function () {
          if (!!entity.listedItemss)
            entity.listedItemss.forEach(item => s.checkedItems[item.id] = true);

        }, 1);

        s.load = function (id) {
          Room.get({ id: id }, function (result) {
            s.room = result;
            result.listedItemss.forEach(item => {
              s.checkedItems[item.id] = true;
            });
          });
        };

        var onSaveSuccess = function (result) {
          s.$emit('reservationsApp:roomUpdate', result);
          $uibModalInstance.close(result);
          s.isSaving = false;
        };

        var onSaveError = function (result) {
          s.isSaving = false;
        };

        s.save = function () {
          Principal.identity().then(res => {
            const { userInput } = s.room
            if (userInput === undefined || userInput === null)
              s.room.userInput = res.login;

            s.isSaving = true;
            for (let k in s.checkedItems) {
              let val = s.checkedItems[k];
              if (val) {
                s.room.listedItemss.push(s.items.find(i => i.id + '' === k + ''))
              }
            }

            if (s.room.id != null) {
              Room.update(s.room, onSaveSuccess, onSaveError);
            } else {
              Room.save(s.room, onSaveSuccess, onSaveError);
            }
          })
        };

        s.clear = function () {
          $uibModalInstance.dismiss('cancel');
        };
        s.datePickerForTimeInput = {};

        s.datePickerForTimeInput.status = {
          opened: false
        };

        s.datePickerForTimeInputOpen = function ($event) {
          s.datePickerForTimeInput.status.opened = true;
        };

        s.setModel = function (item, state, index) {
          console.log(state, item)
          let m = s.room.listedItemss = s.room.listedItemss || [];

          if (state) m.push(item);
          else {
            let idx = m.findIndex(i => i.id === item.id);
            m.slice(idx, 1);
          }
        }

      }
    ]
  );

interface RoomDialogController extends ngController.IScopeExtended {
  load(this: this, id: number)
  save(this: this): void;
  clear(this: this): void;
  setModel(item: Rv.ItemDB, state: boolean, index: number): void;
  room: Rv.RoomDB;
  items: Rv.ItemDB[];

  checkedItems?: { [itemId: string]: boolean }
};