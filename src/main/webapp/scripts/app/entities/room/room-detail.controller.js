'use strict';

angular.module('reservationsApp')
    .controller('RoomDetailController', function ($scope, $rootScope, $stateParams, entity, Room, Item) {
        $scope.room = entity;
        $scope.load = function (id) {
            Room.get({id: id}, function(result) {
                $scope.room = result;
            });
        };
        var unsubscribe = $rootScope.$on('reservationsApp:roomUpdate', function(event, result) {
            $scope.room = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
