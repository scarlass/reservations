"use strict";
if (typeof window.nw !== "undefined") {
    console.log("NWJS detected.");
    var nmain = window.nw.Window.get();
    if (!!nmain.showDevTools) {
        nmain.showDevTools();
    }
}
window.Rv = (function () {
    let StatusCode;
    (function (StatusCode) {
        StatusCode[StatusCode["PENDING"] = 0] = "PENDING";
        StatusCode[StatusCode["ACCEPTED"] = 1] = "ACCEPTED";
        StatusCode[StatusCode["FINISH"] = 2] = "FINISH";
        StatusCode[StatusCode["CANCELED"] = 3] = "CANCELED";
    })(StatusCode || (StatusCode = {}));
    let EntityType;
    (function (EntityType) {
        EntityType[EntityType["Reservation"] = 0] = "Reservation";
        EntityType[EntityType["Room"] = 1] = "Room";
        EntityType[EntityType["Item"] = 2] = "Item";
    })(EntityType || (EntityType = {}));
    let MessageType;
    (function (MessageType) {
        MessageType[MessageType["UPDATED"] = 0] = "UPDATED";
        MessageType[MessageType["CREATE"] = 1] = "CREATE";
        MessageType[MessageType["DELETE"] = 2] = "DELETE";
    })(MessageType || (MessageType = {}));
    let Status = [
        { id: StatusCode.PENDING, text: StatusCode[0] },
        { id: StatusCode.ACCEPTED, text: StatusCode[1] },
        { id: StatusCode.FINISH, text: StatusCode[2] },
        { id: StatusCode.CANCELED, text: StatusCode[3] },
    ];
    return {
        StatusCode,
        EntityType,
        MessageType,
        Status,
    };
})();
angular
    .module("reservationsApp", [
    "LocalStorageModule",
    "tmh.dynamicLocale",
    "pascalprecht.translate",
    "ngResource",
    "ngCookies",
    "ngAria",
    "ngCacheBuster",
    "ngFileUpload",
    "ui.bootstrap",
    "ui.router",
    "infinite-scroll",
    "angular-loading-bar",
])
    .run(function ($rootScope, $location, $window, $http, $state, $translate, Language, Auth, Principal, ENV, VERSION) {
    var updateTitle = function (titleKey) {
        if (!titleKey && $state.$current.data && $state.$current.data.pageTitle) {
            titleKey = $state.$current.data.pageTitle;
        }
        $translate(titleKey || "global.title").then(function (title) {
            $window.document.title = title;
        });
    };
    $rootScope.ENV = ENV;
    $rootScope.VERSION = VERSION;
    $rootScope.$on("$stateChangeStart", function (event, toState, toStateParams) {
        $rootScope.toState = toState;
        $rootScope.toStateParams = toStateParams;
        let isResolved = Principal.isIdentityResolved();
        if (isResolved) {
            let t = Auth.authorize();
        }
        Principal.identityAccData().then((res) => {
            $rootScope.account = res;
        });
        Language.getCurrent().then(function (language) {
            $translate.use(language);
        });
    });
    $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
        var titleKey = "global.title";
        if (toState.name != "login" && $rootScope.previousStateName) {
            $rootScope.previousStateName = fromState.name;
            $rootScope.previousStateParams = fromParams;
        }
        if (toState.data.pageTitle) {
            titleKey = toState.data.pageTitle;
        }
        updateTitle(titleKey);
    });
    $rootScope.$on("$translateChangeSuccess", function () {
        updateTitle();
    });
    $rootScope.back = function () {
        if ($rootScope.previousStateName === "activate" ||
            $state.get($rootScope.previousStateName) === null) {
            $state.go("home");
        }
        else {
            $state.go($rootScope.previousStateName, $rootScope.previousStateParams);
        }
    };
})
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, $translateProvider, tmhDynamicLocaleProvider, httpRequestInterceptorCacheBusterProvider, AlertServiceProvider) {
    httpRequestInterceptorCacheBusterProvider.setMatchlist([/.*api.*/, /.*protected.*/], true);
    $urlRouterProvider.otherwise("/");
    $stateProvider.state("site", {
        abstract: true,
        views: {
            "navbar@": {
                templateUrl: "scripts/components/navbar/navbar.html",
                controller: "NavbarController",
            },
        },
        resolve: {
            authorize: [
                "Auth",
                function (Auth) {
                    return Auth.authorize();
                },
            ],
            translatePartialLoader: [
                "$translate",
                "$translatePartialLoader",
                function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart("global");
                },
            ],
        },
    });
    $httpProvider.interceptors.push("errorHandlerInterceptor");
    $httpProvider.interceptors.push("authExpiredInterceptor");
    $httpProvider.interceptors.push("authInterceptor");
    $httpProvider.interceptors.push("notificationInterceptor");
    $translateProvider.useLoader("$translatePartialLoader", {
        urlTemplate: "i18n/{lang}/{part}.json",
    });
    $translateProvider.preferredLanguage("en");
    $translateProvider.useCookieStorage();
    $translateProvider.useSanitizeValueStrategy("escaped");
    $translateProvider.addInterpolation("$translateMessageFormatInterpolation");
    tmhDynamicLocaleProvider.localeLocationPattern("bower_components/angular-i18n/angular-locale_{{locale}}.js");
    tmhDynamicLocaleProvider.useCookieStorage();
    tmhDynamicLocaleProvider.storageKey("NG_TRANSLATE_LANG_KEY");
})
    .config([
    "$urlMatcherFactoryProvider",
    function ($urlMatcherFactory) {
        $urlMatcherFactory.type("boolean", {
            name: "boolean",
            decode: function (val) {
                return val == true ? true : val == "true" ? true : false;
            },
            encode: function (val) {
                return (val ? 1 : 0);
            },
            equals: function (a, b) {
                return this.is(a) && a === b;
            },
            is: function (val) {
                return [true, false, 0, 1].indexOf(val) >= 0;
            },
            pattern: /bool|true|0|1/,
        });
    },
])
    .directive({
    ngRef: function () {
        return {
            restrict: "A",
            link: function (scp, el, atr) {
                scp.$ref = scp.$ref || {};
                let count = scp.$ref.count;
                scp.$ref.count = typeof count !== "undefined" ? count : 0;
                let refName = atr.ngRef;
                if (refName === "")
                    refName = "ref" + count;
                scp.$ref[refName] = el;
                count++;
            },
        };
    },
});
window.ngApp = angular.module("reservationsApp");
function UppercaseFirstLetter(text) {
    if (typeof text !== "string")
        return "";
    let t = text.toLowerCase();
    return t[0].toUpperCase() + t.slice(1);
}
function isClass(fn) {
    if (typeof fn !== "function")
        return false;
    if (fn.prototype === undefined)
        return false;
    let props = Object.getOwnPropertyNames(fn.prototype);
    let length = props.length;
    let ctorStr = fn.toString().substring(0, 5) === 'class';
    let thisInCtor = /(this)\./.test(fn.toString());
    let isExtnClass = false;
    try {
        let m = new fn;
        isExtnClass = true;
    }
    catch (err) {
        if (err)
            return (isExtnClass = false);
    }
    ;
    return props[0] === 'constructor' && (isExtnClass || ctorStr || length >= 2 || thisInCtor);
}
function isNull(o) {
    return ({}).toString.call(o) === '[object Null]';
}
function isDefined(o) {
    return typeof o !== 'undefined' && !isNull(o);
}
function isFn(o) {
    return typeof o === 'function';
}
function isArr(o) {
    return Array.isArray(o);
}
