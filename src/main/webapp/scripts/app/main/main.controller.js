/// <reference path="../../Main.d.ts" />
'use strict';

angular.module('reservationsApp')
  .controller('MainController', function (
    $scope,
    /** @type {ngController.IRootScope} */
    $rootScope,
    /** @type {ngService.IPrincipal} */
    Principal,
    User
  ) {

    Principal.identityAccData().then(res => {
      $scope.account = res;
      $scope.isAuthenticated = Principal.isAuthenticated;
    });

    // User.query(function (result) {
    //   console.log(result)
    // })
    // log(Principal.identityAccData().then(res => console.log(res)))
  });
