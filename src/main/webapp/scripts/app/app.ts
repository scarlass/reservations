/// <reference path="../Main.d.ts" />
"use strict";

// import "./app.constants";
// import "./"

if (typeof window.nw !== "undefined") {
  console.log("NWJS detected.");
  var nmain = window.nw.Window.get();

  if (!!nmain.showDevTools) {
    nmain.showDevTools();
  }
}

window.Rv = (function () {
  enum StatusCode {
    PENDING = 0,
    ACCEPTED = 1,
    FINISH = 2,
    CANCELED = 3,
  }

  enum EntityType {
    Reservation = 0,
    Room = 1,
    Item = 2,
  }

  enum MessageType {
    UPDATED = 0,
    CREATE = 1,
    DELETE = 2
  }

  let Status: Rv.Status[] = [
    { id: StatusCode.PENDING, text: StatusCode[0] },
    { id: StatusCode.ACCEPTED, text: StatusCode[1] },
    { id: StatusCode.FINISH, text: StatusCode[2] },
    { id: StatusCode.CANCELED, text: StatusCode[3] },
  ];

  return {
    StatusCode,
    EntityType,
    MessageType,
    Status,
  };
})();

angular
  .module("reservationsApp", [
    "LocalStorageModule",
    "tmh.dynamicLocale",
    "pascalprecht.translate",
    "ngResource",
    "ngCookies",
    "ngAria",
    "ngCacheBuster",
    "ngFileUpload",
    // jhipster-needle-angularjs-add-module JHipster will add new module here
    "ui.bootstrap",
    "ui.router",
    "infinite-scroll",
    "angular-loading-bar",
  ])
  .run(function (
    $rootScope: ngController.IRootScope,
    $location,
    $window,
    $http,
    $state,
    $translate: ng.translate.ITranslateService,
    Language,
    Auth,
    Principal: ngService.IPrincipal,
    ENV: string,
    VERSION: string
  ) {
    // update the window title using params in the following
    // precendence
    // 1. titleKey parameter
    // 2. $state.$current.data.pageTitle (current state page title)
    // 3. 'global.title'
    var updateTitle = function (titleKey?: string) {
      if (!titleKey && $state.$current.data && $state.$current.data.pageTitle) {
        titleKey = $state.$current.data.pageTitle;
      }
      $translate(titleKey || "global.title").then(function (title) {
        $window.document.title = title;
      });
    };

    $rootScope.ENV = ENV;
    $rootScope.VERSION = VERSION;
    $rootScope.$on(
      "$stateChangeStart",
      function (event, toState, toStateParams) {
        $rootScope.toState = toState;
        $rootScope.toStateParams = toStateParams;
        let isResolved = Principal.isIdentityResolved();

        if (isResolved) {
          let t = Auth.authorize();
        }

        Principal.identityAccData().then((res) => {
          $rootScope.account = res;
        });

        // Update the language
        Language.getCurrent().then(function (language) {
          $translate.use(language);
        });
      }
    );

    $rootScope.$on(
      "$stateChangeSuccess",
      function (event, toState, toParams, fromState, fromParams) {
        var titleKey = "global.title";

        // Remember previous state unless we've been redirected to login or we've just
        // reset the state memory after logout. If we're redirected to login, our
        // previousState is already set in the authExpiredInterceptor. If we're going
        // to login directly, we don't want to be sent to some previous state anyway
        if (toState.name != "login" && $rootScope.previousStateName) {
          $rootScope.previousStateName = fromState.name;
          $rootScope.previousStateParams = fromParams;
        }

        // Set the page title key to the one configured in state or use default one
        if (toState.data.pageTitle) {
          titleKey = toState.data.pageTitle;
        }
        updateTitle(titleKey);
      }
    );

    // if the current translation changes, update the window title
    $rootScope.$on("$translateChangeSuccess", function () {
      updateTitle();
    });

    $rootScope.back = function () {
      // If previous state is 'activate' or do not exist go to 'home'
      if (
        $rootScope.previousStateName === "activate" ||
        $state.get($rootScope.previousStateName) === null
      ) {
        $state.go("home");
      } else {
        $state.go($rootScope.previousStateName, $rootScope.previousStateParams);
      }
    };
  })
  .config(function (
    $stateProvider: angular.ui.IStateProvider,
    $urlRouterProvider: angular.ui.IUrlRouterProvider,
    $httpProvider: any,
    $locationProvider: ng.ILocationProvider,
    $translateProvider: ng.translate.ITranslateProvider,
    tmhDynamicLocaleProvider,
    httpRequestInterceptorCacheBusterProvider,
    AlertServiceProvider
  ) {
    // uncomment below to make alerts look like toast
    //AlertServiceProvider.showAsToast(true);

    //Cache everything except rest api requests
    httpRequestInterceptorCacheBusterProvider.setMatchlist(
      [/.*api.*/, /.*protected.*/],
      true
    );

    $urlRouterProvider.otherwise("/");
    $stateProvider.state("site", {
      abstract: true,
      views: {
        "navbar@": {
          templateUrl: "scripts/components/navbar/navbar.html",
          controller: "NavbarController",
        },
      },
      resolve: {
        authorize: [
          "Auth",
          function (Auth) {
            return Auth.authorize();
          },
        ],
        translatePartialLoader: [
          "$translate",
          "$translatePartialLoader",
          function ($translate, $translatePartialLoader) {
            $translatePartialLoader.addPart("global");
          },
        ],
      },
    });

    $httpProvider.interceptors.push("errorHandlerInterceptor");
    $httpProvider.interceptors.push("authExpiredInterceptor");
    $httpProvider.interceptors.push("authInterceptor");
    $httpProvider.interceptors.push("notificationInterceptor");
    // jhipster-needle-angularjs-add-interceptor JHipster will add new application interceptor here

    // Initialize angular-translate
    $translateProvider.useLoader("$translatePartialLoader", {
      urlTemplate: "i18n/{lang}/{part}.json",
    });

    $translateProvider.preferredLanguage("en");
    $translateProvider.useCookieStorage();
    $translateProvider.useSanitizeValueStrategy("escaped");
    $translateProvider.addInterpolation("$translateMessageFormatInterpolation");

    tmhDynamicLocaleProvider.localeLocationPattern(
      "bower_components/angular-i18n/angular-locale_{{locale}}.js"
    );
    tmhDynamicLocaleProvider.useCookieStorage();
    tmhDynamicLocaleProvider.storageKey("NG_TRANSLATE_LANG_KEY");
  })
  // jhipster-needle-angularjs-add-config JHipster will add new application configuration here
  .config([
    "$urlMatcherFactoryProvider",
    function ($urlMatcherFactory: angular.ui.IUrlMatcherFactory) {
      $urlMatcherFactory.type<boolean>("boolean", {
        name: "boolean",
        decode: function (val) {
          //@ts-ignore
          return val == true ? true : val == "true" ? true : false;
        },
        encode: function (val) {
          return (val ? 1 : 0) as any;
        },
        equals: function (a, b) {
          return this.is(a) && a === b;
        },
        is: function (val) {
          return [true, false, 0, 1].indexOf(val) >= 0;
        },
        pattern: /bool|true|0|1/,
      });
    },
  ])
  .directive({
    ngRef: function () {
      return {
        restrict: "A",
        link: function (scp, el, atr) {
          scp.$ref = scp.$ref || {};

          let count = scp.$ref.count as number;
          scp.$ref.count = typeof count !== "undefined" ? count : 0;

          let refName = atr.ngRef as string;
          if (refName === "") refName = "ref" + count;

          scp.$ref[refName] = el;
          count++;
        },
      };
    },
  });


window.ngApp = angular.module("reservationsApp");

function UppercaseFirstLetter(text: string) {
  if (typeof text !== "string") return "";
  let t = text.toLowerCase();
  return t[0].toUpperCase() + t.slice(1);
}

function isClass(fn: FunctionConstructor) {
  if (typeof fn !== "function") return false;
  if (fn.prototype === undefined) return false;
  let props = Object.getOwnPropertyNames(fn.prototype);
  let length = props.length;

  let ctorStr = fn.toString().substring(0, 5) === 'class';
  let thisInCtor = /(this)\./.test(fn.toString());
  let isExtnClass = false;

  try {
    let m = new fn;
    isExtnClass = true
  }
  catch (err) {
    if (err)
      return (isExtnClass = false);

  };



  return props[0] === 'constructor' && (isExtnClass || ctorStr || length >= 2 || thisInCtor);
}
function isNull(o: any): o is null {
  return ({}).toString.call(o) === '[object Null]';
}
function isDefined<T>(o: T): o is Exclude<T, undefined | null> {
  return typeof o !== 'undefined' && !isNull(o);
}
function isFn(o: any): o is Function {
  return typeof o === 'function'
}
function isArr(o: any): o is any[] {
  return Array.isArray(o);
}
