package app.domain;

import java.time.ZonedDateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A UpdateLog.
 */
@Entity
@Table(name = "update_log")
public class UpdateLog implements Serializable {

  public static class EntityType {
    public static final Integer Reservation = 0;
    public static final Integer Room = 1;
    public static final Integer Item = 2;

    public static String type(Integer delta) {
      switch(delta) {
        case 0: return "Reservation";
        case 1: return "Room";
        case 2: return "Item";
        default: return null;
      }
    }
  }

  public static class MessageType {
    public static final Integer UPDATED = 0;
    public static final Integer CREATE = 1;
    public static final Integer DELETE = 2;

    public static String type(Integer delta) {
      switch(delta) {
        case 0: return "UPDATED";
        case 1: return "CREATE";
        case 2: return "DELETE";
        default: return null;
      }
    }

  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "message")
  private String message;

  @Column(name = "message_type")
  private Integer messageType;

  @Lob
  @Column(name = "additional_message")
  private String additionalMessage;

  @Column(name = "entity_type")
    private Integer entityType;

  @Column(name = "time_input")
  private ZonedDateTime timeInput;

  @ManyToOne
  @JoinColumn(name = "reservation_id")
  private Reservation reservation;

  @ManyToOne
  @JoinColumn(name = "room_id")
  private Room room;

  @ManyToOne
  @JoinColumn(name = "item_id")
  private Item item;

  @Column(name = "dummy_id")
  private Long dummyId;

  public Long getId() {
        return id;
    }
  public void setId(Long id) {
    this.id = id;
  }

  public String getMessage() {
        return message;
    }
  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getMessageType() {
        return messageType;
    }
  public void setMessageType(Integer messageType) {
    this.messageType = messageType;
  }

  public String getAdditionalMessage() {
        return additionalMessage;
    }
  public void setAdditionalMessage(String additionalMessage) {
    this.additionalMessage = additionalMessage;
  }

  public Integer getEntityType() {
        return entityType;
    }
  public void setEntityType(Integer entityType) {
    this.entityType = entityType;
  }

  public ZonedDateTime getTimeInput() {
        return timeInput;
    }
  public void setTimeInput(ZonedDateTime timeInput) {
    this.timeInput = timeInput;
  }

  public Reservation getReservation() {
        return reservation;
    }
  public void setReservation(Reservation reservation) {
    this.reservation = reservation;
  }

  public Room getRoom() {
        return room;
    }
  public void setRoom(Room room) {
    this.room = room;
  }

  public Item getItem() {
        return item;
    }
  public void setItem(Item item) {
    this.item = item;
  }

  public Long getDummyId() {
    return dummyId;
  }
  public void setDummyId(Long dummyId) {
    this.dummyId = dummyId;
  }

  public <T extends Serializable> void createUpdateLog(T data, Integer messageType) {
    boolean isCreateMessage = MessageType.CREATE.equals(messageType);
    this.messageType = messageType;

    if(data instanceof Reservation) {
      entityType = Reservation.type;
      reservation = (Reservation) data;
      createOrUpdateLog(reservation.getId(), reservation.getUserLogin(), isCreateMessage);
    }
    else if(data instanceof Item) {
      entityType = Item.type;
      item = (Item) data;
      createOrUpdateLog(item.getId(), item.getUserInput(), isCreateMessage);
    }
    else if(data instanceof Room) {
      entityType = Room.type;
      room = (Room) data;
      createOrUpdateLog(room.getId(), room.getUserInput(), isCreateMessage);
    }

  }

  private void createOrUpdateLog(Long id, String user, boolean isCreateMessage) {
      dummyId = id;
      if(isCreateMessage)
        message =  createMessage(id, user);
      else
        message = updatedMessage(id);
  }

  public void deleteLog(Integer entityType, Integer messageType, Long entityId) {
    dummyId = entityId;
    this.messageType = messageType;
    this.entityType = entityType;
    this.message = deleteMessage(entityId);
  }

  private String createMessage(Long id, String user) {
    return EntityType.type(entityType) +
      " Id: " + id + ", User: " + user;
  }
  private String deleteMessage(Long id) {
    return EntityType.type(entityType) + " Id: " + id;
  }
  private String updatedMessage(Long id) {
    return EntityType.type(entityType) + " Id: " + id;
  }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UpdateLog updateLog = (UpdateLog) o;
        if(updateLog.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, updateLog.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "UpdateLog{" +
            "id=" + id +
            ", message='" + message + "'" +
            ", messageType='" + messageType + "'" +
            ", additionalMessage='" + additionalMessage + "'" +
            ", entityType='" + entityType + "'" +
            ", timeInput='" + timeInput + "'" +
            '}';
    }
}
