package app.domain;

import java.time.ZonedDateTime;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Reservation.
 */
@Entity
@Table(name = "reservation")
public class Reservation implements Serializable {
  public static final Integer type = 0;

  public static class Status {
    public static final Integer PENDING = 0;
    public static final Integer ACCEPTED = 1;
    public static final Integer FINISH = 2;
    public static final Integer CANCELED = 3;

  }


  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "user_login")
  private String userLogin;

  @NotNull
  @Column(name = "usage_time_start", nullable = false)
  private ZonedDateTime usageTimeStart;

  @NotNull
  @Column(name = "usage_time_end", nullable = false)
  private ZonedDateTime usageTimeEnd;

  @Lob
  @Column(name = "details")
  private String details;

  @Column(name = "time_input")
  private ZonedDateTime timeInput;

  @Column(name = "status")
  private Integer status;

  @ManyToOne
  @JoinColumn(name = "room_id")
  private Room room;

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  @Column(name = "room_update_info")
  private String roomUpdateInfo;

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
        this.id = id;
    }

  public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
  public String getUserLogin() {
    return userLogin;
  }

  public ZonedDateTime getUsageTimeStart() {
    return usageTimeStart;
  }
  public void setUsageTimeStart(ZonedDateTime usageTimeStart) {
    this.usageTimeStart = usageTimeStart;
  }

  public ZonedDateTime getUsageTimeEnd() {
    return usageTimeEnd;
  }
  public void setUsageTimeEnd(ZonedDateTime usageTimeEnd) {
    this.usageTimeEnd = usageTimeEnd;
  }

  public String getDetails() {
    return details;
  }
  public void setDetails(String details) {
        this.details = details;
    }

  public ZonedDateTime getTimeInput() {
        return timeInput;
    }
  public void setTimeInput(ZonedDateTime timeInput) {
    this.timeInput = timeInput;
  }

  public Integer getStatus() {
        return status;
    }
  public void setStatus(Integer status) {
    this.status = status;
  }

  public Room getRoom() {
        return room;
    }
  public void setRoom(Room room) {
    this.room = room;
  }

  public User getUser() {
        return user;
    }
  public void setUser(User user) {
    this.user = user;
  }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reservation reservation = (Reservation) o;
        if(reservation.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, reservation.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Reservation{" +
            "id=" + id +
            ", userLogin='" + userLogin + "'" +
            ", usageTimeStart='" + usageTimeStart + "'" +
            ", usageTimeEnd='" + usageTimeEnd + "'" +
            ", details='" + details + "'" +
            ", timeInput='" + timeInput + "'" +
            ", status='" + status + "'" +
            '}';
    }

  public String getRoomUpdateInfo() {
    return roomUpdateInfo;
  }
  public void setRoomUpdateInfo(String roomUpdateInfo) {
    this.roomUpdateInfo = roomUpdateInfo;
  }
}
