package app.domain;

import java.time.ZonedDateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Room.
 */
@Entity
@Table(name = "room")
public class Room implements Serializable {
    public static final Integer type = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "room_name")
    private String roomName;

    @Column(name = "user_input")
    private String userInput;

    @Column(name = "time_input")
    private ZonedDateTime timeInput;

    @ManyToMany
    @JoinTable(name = "room_listed_items",
               joinColumns = @JoinColumn(name="rooms_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="listed_itemss_id", referencedColumnName="ID"))
    private Set<Item> listedItemss = new HashSet<>();

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomName() {
        return roomName;
    }
    public void setRoomName(String roomName) {
      this.roomName = roomName;
    }

    public String getUserInput() {
        return userInput;
    }
    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }

    public ZonedDateTime getTimeInput() {
        return timeInput;
    }
    public void setTimeInput(ZonedDateTime timeInput) {
        this.timeInput = timeInput;
    }

    public Set<Item> getListedItemss() {
        return listedItemss;
    }
    public void setListedItemss(Set<Item> items) {
        this.listedItemss = items;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Room room = (Room) o;
        if(room.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, room.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Room{" +
            "id=" + id +
            ", roomName='" + roomName + "'" +
            ", userInput='" + userInput + "'" +
            ", timeInput='" + timeInput + "'" +
            '}';
    }

}
