package app.web.rest.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Room entity.
 */
public class RoomDTO implements Serializable {

    private Long id;

    private String roomName;


    private String userInput;


    private ZonedDateTime timeInput;


    private Set<ItemDTO> listedItemss = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
    public String getUserInput() {
        return userInput;
    }

    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }
    public ZonedDateTime getTimeInput() {
        return timeInput;
    }

    public void setTimeInput(ZonedDateTime timeInput) {
        this.timeInput = timeInput;
    }

    public Set<ItemDTO> getListedItemss() {
        return listedItemss;
    }

    public void setListedItemss(Set<ItemDTO> items) {
        this.listedItemss = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RoomDTO roomDTO = (RoomDTO) o;

        if ( ! Objects.equals(id, roomDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "RoomDTO{" +
            "id=" + id +
            ", roomName='" + roomName + "'" +
            ", userInput='" + userInput + "'" +
            ", timeInput='" + timeInput + "'" +
            '}';
    }
}
