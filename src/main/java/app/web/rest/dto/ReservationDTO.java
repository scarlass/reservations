package app.web.rest.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;


/**
 * A DTO for the Reservation entity.
 */
public class ReservationDTO implements Serializable {

    private Long id;

    private String userLogin;


    @NotNull
    private ZonedDateTime usageTimeStart;


    @NotNull
    private ZonedDateTime usageTimeEnd;


    @Lob
    private String details;


    private ZonedDateTime timeInput;


    private Integer status;


    private Long roomId;
    private Long userId;

    private String roomName;
    private String roomUpdateInfo;

    public Long getId() { return id;
    }
    public void setId(Long id) { this.id = id;
    }
    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
    public ZonedDateTime getUsageTimeStart() {
        return usageTimeStart;
    }

    public void setUsageTimeStart(ZonedDateTime usageTimeStart) {
        this.usageTimeStart = usageTimeStart;
    }
    public ZonedDateTime getUsageTimeEnd() {
        return usageTimeEnd;
    }

    public void setUsageTimeEnd(ZonedDateTime usageTimeEnd) {
        this.usageTimeEnd = usageTimeEnd;
    }
    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
    public ZonedDateTime getTimeInput() {
        return timeInput;
    }

    public void setTimeInput(ZonedDateTime timeInput) {
        this.timeInput = timeInput;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReservationDTO reservationDTO = (ReservationDTO) o;

        if ( ! Objects.equals(id, reservationDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ReservationDTO{" +
            "id=" + id +
            ", userLogin='" + userLogin + "'" +
            ", usageTimeStart='" + usageTimeStart + "'" +
            ", usageTimeEnd='" + usageTimeEnd + "'" +
            ", details='" + details + "'" +
            ", timeInput='" + timeInput + "'" +
            ", status='" + status + "'" +
            '}';
    }

  public String getRoomName() {
    return roomName;
  }
  public void setRoomName(String roomName) {
    this.roomName = roomName;
  }

  public String getRoomUpdateInfo() {
    return roomUpdateInfo;
  }
  public void setRoomUpdateInfo(String roomUpdateInfo) {
    this.roomUpdateInfo = roomUpdateInfo;
  }
}
