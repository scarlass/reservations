package app.web.rest.dto;

import app.domain.UpdateLog;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;


/**
 * A DTO for the UpdateLog entity.
 */
public class UpdateLogDTO implements Serializable {

  @Lob
  private String additionalMessage;
  private Long id;
  private String message;
  private Integer messageType;
  private Integer entityType;
  private ZonedDateTime timeInput;
  private Long reservationId;
  private Long roomId;
  private Long itemId;
  private Long dummyId;

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
        this.id = id;
    }

  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
        this.message = message;
    }

  public Integer getMessageType() {
    return messageType;
  }
  public void setMessageType(Integer messageType) {
        this.messageType = messageType;
    }

  public String getAdditionalMessage() {
    return additionalMessage;
  }
  public void setAdditionalMessage(String additionalMessage) {
        this.additionalMessage = additionalMessage;
    }

  public Integer getEntityType() {
    return entityType;
  }
  public void setEntityType(Integer entityType) {
      this.entityType = entityType;
    }

  public ZonedDateTime getTimeInput() {
    return timeInput;
  }
  public void setTimeInput(ZonedDateTime timeInput) {
        this.timeInput = timeInput;
    }

  public Long getReservationId() {
        return reservationId;
    }
  public void setReservationId(Long reservationId) {
    this.reservationId = reservationId;
  }

  public Long getRoomId() {
        return roomId;
    }
  public void setRoomId(Long roomId) {
    this.roomId = roomId;
  }

  public Long getItemId() {
        return itemId;
    }
  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Long getDummyId() { return dummyId; }
  public void setDummyId(Long dummyId) { this.dummyId = dummyId; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    UpdateLogDTO updateLogDTO = (UpdateLogDTO) o;

    if ( ! Objects.equals(id, updateLogDTO.id)) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override
  public String toString() {
    return "UpdateLogDTO{" +
      "id=" + id +
      ", message='" + message + "'" +
      ", messageType='" + messageType + "'" +
      ", additionalMessage='" + additionalMessage + "'" +
      ", entityType='" + entityType + "'" +
      ", timeInput='" + timeInput + "'" +
      '}';
  }
}
