package app.web.rest;

import com.codahale.metrics.annotation.Timed;
import app.domain.UpdateLog;
import app.service.UpdateLogService;
import app.web.rest.util.HeaderUtil;
import app.web.rest.util.PaginationUtil;
import app.web.rest.dto.UpdateLogDTO;
import app.web.rest.mapper.UpdateLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing UpdateLog.
 */
@RestController
@RequestMapping("/api")
public class UpdateLogResource {

    private final Logger log = LoggerFactory.getLogger(UpdateLogResource.class);
        
    @Inject
    private UpdateLogService updateLogService;
    
    @Inject
    private UpdateLogMapper updateLogMapper;
    
    /**
     * POST  /updateLogs -> Create a new updateLog.
     */
    @RequestMapping(value = "/updateLogs",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UpdateLogDTO> createUpdateLog(@RequestBody UpdateLogDTO updateLogDTO) throws URISyntaxException {
        log.debug("REST request to save UpdateLog : {}", updateLogDTO);
        if (updateLogDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("updateLog", "idexists", "A new updateLog cannot already have an ID")).body(null);
        }
        UpdateLogDTO result = updateLogService.save(updateLogDTO);
        return ResponseEntity.created(new URI("/api/updateLogs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("updateLog", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /updateLogs -> Updates an existing updateLog.
     */
    @RequestMapping(value = "/updateLogs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UpdateLogDTO> updateUpdateLog(@RequestBody UpdateLogDTO updateLogDTO) throws URISyntaxException {
        log.debug("REST request to update UpdateLog : {}", updateLogDTO);
        if (updateLogDTO.getId() == null) {
            return createUpdateLog(updateLogDTO);
        }
        UpdateLogDTO result = updateLogService.save(updateLogDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("updateLog", updateLogDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /updateLogs -> get all the updateLogs.
     */
    @RequestMapping(value = "/updateLogs",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<UpdateLogDTO>> getAllUpdateLogs(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of UpdateLogs");
        Page<UpdateLog> page = updateLogService.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/updateLogs");
        return new ResponseEntity<>(page.getContent().stream()
            .map(updateLogMapper::updateLogToUpdateLogDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /updateLogs/:id -> get the "id" updateLog.
     */
    @RequestMapping(value = "/updateLogs/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UpdateLogDTO> getUpdateLog(@PathVariable Long id) {
        log.debug("REST request to get UpdateLog : {}", id);
        UpdateLogDTO updateLogDTO = updateLogService.findOne(id);
        return Optional.ofNullable(updateLogDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /updateLogs/:id -> delete the "id" updateLog.
     */
    @RequestMapping(value = "/updateLogs/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUpdateLog(@PathVariable Long id) {
        log.debug("REST request to delete UpdateLog : {}", id);
        updateLogService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("updateLog", id.toString())).build();
    }
}
