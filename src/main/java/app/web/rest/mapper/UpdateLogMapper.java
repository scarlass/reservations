package app.web.rest.mapper;

import app.domain.*;
import app.web.rest.dto.UpdateLogDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UpdateLog and its DTO UpdateLogDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UpdateLogMapper {

    @Mapping(source = "reservation.id", target = "reservationId")
    @Mapping(source = "room.id", target = "roomId")
    @Mapping(source = "item.id", target = "itemId")
    UpdateLogDTO updateLogToUpdateLogDTO(UpdateLog updateLog);

    @Mapping(source = "reservationId", target = "reservation")
    @Mapping(source = "roomId", target = "room")
    @Mapping(source = "itemId", target = "item")
    UpdateLog updateLogDTOToUpdateLog(UpdateLogDTO updateLogDTO);

    default Reservation reservationFromId(Long id) {
        if (id == null) {
            return null;
        }
        Reservation reservation = new Reservation();
        reservation.setId(id);
        return reservation;
    }

    default Room roomFromId(Long id) {
        if (id == null) {
            return null;
        }
        Room room = new Room();
        room.setId(id);
        return room;
    }

    default Item itemFromId(Long id) {
        if (id == null) {
            return null;
        }
        Item item = new Item();
        item.setId(id);
        return item;
    }
}
