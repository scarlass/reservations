package app.web.rest.mapper;

import app.domain.*;
import app.web.rest.dto.ReservationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Reservation and its DTO ReservationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReservationMapper {

    @Mapping(source = "room.id", target = "roomId")
    @Mapping(source = "room.roomName", target = "roomName")
    @Mapping(source = "user.id", target = "userId")
    ReservationDTO reservationToReservationDTO(Reservation reservation);

    @Mapping(source = "roomId", target = "room")
    @Mapping(source = "userId", target = "user")
    Reservation reservationDTOToReservation(ReservationDTO reservationDTO);

    default Room roomFromId(Long id) {
        if (id == null) {
            return null;
        }
        Room room = new Room();
        room.setId(id);
        return room;
    }

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
