package app.web.rest;

import app.domain.Room;
import app.domain.User;
import app.repository.ReservationRepository;
import app.service.RoomService;
import app.service.UserService;
import com.codahale.metrics.annotation.Timed;
import app.domain.Reservation;
import app.service.ReservationService;
import app.web.rest.util.HeaderUtil;
import app.web.rest.util.PaginationUtil;
import app.web.rest.dto.ReservationDTO;
import app.web.rest.mapper.ReservationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Reservation.
 */
@RestController
@RequestMapping("/api")
public class ReservationResource {

  private final Logger log = LoggerFactory.getLogger(ReservationResource.class);

  @Inject
  private ReservationService reservationService;

  @Inject
  private ReservationMapper reservationMapper;

  @Inject
  private UserService userService;

  @Inject
  private ReservationRepository reservationRepository;

  @RequestMapping(value = "/reservations",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<ReservationDTO> createReservation(@Valid @RequestBody ReservationDTO reservationDTO) throws URISyntaxException {
    log.debug("REST request to save Reservation : {}", reservationDTO);
    if (reservationDTO.getId() != null) {
      return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("reservation", "idexists", "A new reservation cannot already have an ID")).body(null);
    }
    ReservationDTO result = reservationService.save(reservationDTO);
    return ResponseEntity.created(new URI("/api/reservations/" + result.getId()))
      .headers(HeaderUtil.createEntityCreationAlert("reservation", result.getId().toString()))
      .body(result);
  }

  @RequestMapping(value = "/reservations",
    method = RequestMethod.PUT,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<ReservationDTO> updateReservation(@Valid @RequestBody ReservationDTO reservationDTO) throws URISyntaxException {
    log.debug("REST request to update Reservation : {}", reservationDTO);
    if (reservationDTO.getId() == null) {
      return createReservation(reservationDTO);
    }
    ReservationDTO result = reservationService.save(reservationDTO, true);
    return ResponseEntity.ok()
      .headers(HeaderUtil.createEntityUpdateAlert("reservation", reservationDTO.getId().toString()))
      .body(result);
  }

  @RequestMapping(value = "/reservations",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Timed
  @Transactional(readOnly = true)
  public ResponseEntity<List<ReservationDTO>> getAllReservations(Pageable pageable)
    throws URISyntaxException {
    log.debug("REST request to get a page of Reservations");
    Page<Reservation> page = reservationService.findAll(pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/reservations");

    return new ResponseEntity<>(page.getContent().stream()
      .map(reservationMapper::reservationToReservationDTO)
      .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
  }


  @RequestMapping(value = "/reservations/{id}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<ReservationDTO> getReservation(@PathVariable Long id, @RequestParam(name = "login", required = false) String userLogin) {
    log.debug("REST request to get Reservation : {}", id);
    ReservationDTO reservationDTO = reservationService.findOne(id);
    return Optional.ofNullable(reservationDTO)
      .map(result -> new ResponseEntity<>(
        result,
        HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @RequestMapping(value = "/reservations/{id}",
    method = RequestMethod.DELETE,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Timed
  public ResponseEntity<Void> deleteReservation(@PathVariable Long id) {
    log.debug("REST request to delete Reservation : {}", id);
    reservationService.delete(id);
    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("reservation", id.toString())).build();
  }

  @RequestMapping(
    value = "/reservations/search",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Timed
  public ResponseEntity<List<ReservationDTO>> getPageByKeyword(
    @RequestParam(name = "searchType") String searchType,
    @RequestParam(name = "userLogin", required = false) String userLogin,
    @RequestParam(name = "key", required = false) String key,
    @RequestParam(name = "status", required = false) Long status,
    Pageable pageable
  ) throws URISyntaxException {
      // REMINDER SEARCH TYPE VALUE: login, room;
    Page<Reservation> page = reservationService.findAll(pageable);
    log.debug("\n\n\tSearch Type:" + searchType + "\n\n");

    if(searchType.equals("status") && status != null) {
      if(userLogin != null)
        page = reservationService.findAllByStatus(userLogin, status.intValue(), pageable);
      else
        page = reservationService.findAllByStatus(status.intValue(), pageable);
    }
    else {
      if(userLogin != null)
        page = reservationService.findDataByKeyword(searchType, userLogin, key, pageable);
      else
        page = reservationService.findDataByKeyword(searchType, key, pageable);
    }

    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/reservations");

    return new ResponseEntity<>(
      page.getContent().stream().map(reservationMapper::reservationToReservationDTO).collect(Collectors.toCollection(LinkedList::new)),
      headers, HttpStatus.OK
    );

  }

}
