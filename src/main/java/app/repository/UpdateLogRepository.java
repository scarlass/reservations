package app.repository;

import app.domain.UpdateLog;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the UpdateLog entity.
 */
public interface UpdateLogRepository extends JpaRepository<UpdateLog,Long> {

  List<UpdateLog> findAllByReservationId(Long reservationId);
  List<UpdateLog> findAllByRoomId(Long roomId);
  List<UpdateLog> findAllByItemId(Long itemId);

  void deleteAllByReservationId(Long id);
  void deleteAllByRoomId(Long id);
  void deleteAllByItemId(Long id);

}
