package app.repository;

import app.domain.Item;
import app.domain.Room;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Room entity.
 */
public interface RoomRepository extends JpaRepository<Room, Long> {

  @Query("select distinct room from Room room left join fetch room.listedItemss")
  List<Room> findAllWithEagerRelationships();

  @Query("select room from Room room left join fetch room.listedItemss where room.id =:id")
  Room findOneWithEagerRelationships(@Param("id") Long id);

  Optional<Room> findFirstByRoomName(String roomName);
  Optional<Room> findOneByRoomName(String roomName);

  Page<Room> findAllByRoomNameContainingIgnoreCase(String roomName, Pageable pageAble);

  Optional<Room> findOneByRoomNameContainingIgnoreCase(String roomName);

  @Query("SELECT rm FROM Room rm WHERE rm.roomName LIKE %:roomName%")
  List<Room> findByRoomNameClosestPattern(@Param("roomName") String roomNameLike);

  @Query(
    nativeQuery = true,
    value = "DELETE FROM public.room_listed_items WHERE listed_itemss_id = :itemId"
  )
  void deleteRelationByItemId(@Param("itemId") Long itemId);

  @Query("select distinct rm from Room rm left join fetch rm.listedItemss where :itemObj member of rm.listedItemss")
  List<Room> findAllByItems(@Param("itemObj") Item item);
}
