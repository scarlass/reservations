package app.repository;

import app.domain.Reservation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data JPA repository for the Reservation entity.
 */
public interface ReservationRepository extends JpaRepository<Reservation,Long> {

  @Query("select reservation from Reservation reservation where reservation.user.login = ?#{principal.username}")
  List<Reservation> findByUserIsCurrentUser();

  @Query("select reservation from Reservation reservation where reservation.user.login = ?#{principal.username}")
  Page<Reservation> findByUserIsCurrentUser(Pageable pageable);

  Reservation findOneByRoomId(Long roomId);

  List<Reservation> findAllByRoomId(Long roomId);
  Page<Reservation> findAllByRoomId(Long roomId, Pageable pageable);

  List<Reservation> findAllByUserLoginContainingIgnoreCase(String UserLogin);
  Page<Reservation> findAllByUserLoginContainingIgnoreCase(String userLogin, Pageable pageable);
  Page<Reservation> findAllByUserLogin(String userLogin, Pageable pageable);

  Page<Reservation> findAllByUserLoginContainingIgnoreCaseAndRoomId(String userLogin, Long roomId, Pageable pageable);

  List<Reservation> findAllByUsageTimeStartGreaterThanEqualAndUsageTimeEndLessThanEqual(ZonedDateTime usageTimeStart, ZonedDateTime usageTimeEnd);
  List<Reservation> findAllByUsageTimeEndGreaterThanEqualAndUsageTimeStartLessThanEqual(ZonedDateTime usageTimeStart, ZonedDateTime usageTimeEnd);

  // Start Time
  List<Reservation> findAllByUsageTimeStartGreaterThanEqualAndUsageTimeStartLessThanEqual(ZonedDateTime start, ZonedDateTime end);
  List<Reservation> findAllByUserLoginAndUsageTimeStartGreaterThanEqualAndUsageTimeStartLessThanEqual(String userLogin, ZonedDateTime start, ZonedDateTime end);

  // End Time
  List<Reservation> findAllByUsageTimeEndGreaterThanEqualAndUsageTimeEndLessThanEqual(ZonedDateTime start, ZonedDateTime end);
  List<Reservation> findAllByUserLoginAndUsageTimeEndGreaterThanEqualAndUsageTimeEndLessThanEqual(String userLogin, ZonedDateTime start, ZonedDateTime end);

  Page<Reservation> findAllByUserLoginIgnoreCaseAndRoomUpdateInfoNotNull(String userLogin, Pageable pageable);

  List<Reservation> findAllByStatus(Integer status);
  Page<Reservation> findAllByStatus(Integer status, Pageable pageable);

  List<Reservation> findAllByUserLoginIgnoreCaseAndStatus(String userLogin, Integer status);
  Page<Reservation> findAllByUserLoginIgnoreCaseAndStatus(String userLogin, Integer status, Pageable pageable);
}
