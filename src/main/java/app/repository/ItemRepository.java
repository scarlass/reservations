package app.repository;

import app.domain.Item;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Item entity.
 */
public interface ItemRepository extends JpaRepository<Item,Long> {

  Page<Item> findAllByItemNameContainingIgnoreCase(String itemName, Pageable pageable);
  List<Item> findAllByItemNameContainingIgnoreCase(String itemName);

  Optional<Item> findOneByItemNameContainingIgnoreCase(String itemName);
}
