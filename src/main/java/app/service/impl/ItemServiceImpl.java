package app.service.impl;

import app.domain.Room;
import app.service.ItemService;
import app.domain.Item;
import app.repository.ItemRepository;
import app.security.SecurityUtils;
import app.service.RoomService;
import app.service.UpdateLogService;
import app.web.rest.dto.ItemDTO;
import app.web.rest.mapper.ItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Item.
 */
@Service
@Transactional
public class ItemServiceImpl implements ItemService {

    private final Logger log = LoggerFactory.getLogger(ItemServiceImpl.class);

    @Inject
    private ItemRepository itemRepository;

    @Inject
    private ItemMapper itemMapper;

    @Inject
    private UpdateLogService updateLogService;

    @Inject
    private RoomService roomService;

    /**
     * Save a item.
     * @return the persisted entity
     */
    public ItemDTO save(ItemDTO itemDTO) {
        log.debug("Request to save Item : {}", itemDTO);
        Item item = itemMapper.itemDTOToItem(itemDTO);
        final boolean isNew = item.getId() == null;

        Optional<Item> optItem = itemRepository.findOneByItemNameContainingIgnoreCase(item.getItemName());
        if(isNew && optItem.isPresent()) {
          throw new IllegalArgumentException("Try to use different name.");
        }

        item.setTimeInput(ZonedDateTime.now());
        item = itemRepository.save(item);
        updateLogService.create(item, isNew);
        ItemDTO result = itemMapper.itemToItemDTO(item);
        return result;
    }

    /**
     *  get all the items.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Item> findAll(Pageable pageable) {
        log.debug("Request to get all Items");
        Page<Item> result = itemRepository.findAll(pageable);
        return result;
    }

    /**
     *  get one item by id.
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ItemDTO findOne(Long id) {
        log.debug("Request to get Item : {}", id);
        Item item = itemRepository.findOne(id);
        ItemDTO itemDTO = itemMapper.itemToItemDTO(item);
        return itemDTO;
    }
  public Item findOneById(Long id) {
    log.debug("Request to get Item : {}", id);
    return itemRepository.findOne(id);
  }
    /**
     *  delete the  item by id.
     */
    public void delete(Long id) {
        log.debug("Request to delete Item : {}", id);
        updateLogService.create(id, Item.type);
        roomService.deleteByListedItemContainsItemId(id);
        itemRepository.delete(id);
    }

    public Page<Item> findItemNameByKeyword(String searchType, String key, Pageable pageable) {
      log.debug("\n\tSearchType: "+searchType+"\n");
      if ("ItemName".equals(searchType)) {
        return itemRepository.findAllByItemNameContainingIgnoreCase(key, pageable);
      }
      return itemRepository.findAll(pageable);
    }
}
