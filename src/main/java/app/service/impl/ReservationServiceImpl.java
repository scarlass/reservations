package app.service.impl;

import app.service.ReservationService;
import app.domain.Reservation;
import app.domain.Room;
import app.repository.ReservationRepository;
import app.service.RoomService;
import app.service.UpdateLogService;
import app.service.util.Utils;
import app.web.rest.dto.ReservationDTO;
import app.web.rest.mapper.ReservationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Reservation.
 */
@Service
@Transactional
public class ReservationServiceImpl implements ReservationService{

  private final Logger log = LoggerFactory.getLogger(ReservationServiceImpl.class);

  @Inject
  private ReservationRepository reservationRepository;

  @Inject
  private ReservationMapper reservationMapper;

  @Inject
  RoomService roomService;
  @Inject
  UpdateLogService updateLogService;

  /**
   * Save a reservation.
   * @return the persisted entity
   */
  public ReservationDTO save(ReservationDTO reservationDTO, boolean skipDateCheck) {
    log.debug("Request to save Reservation : {}", reservationDTO);
//    void logDTO = updateLogService.create();
    Reservation reservation = reservationMapper.reservationDTOToReservation(reservationDTO);
    final boolean isNew = reservation.getId() == null;

    if(!skipDateCheck)
      Utils.checkOverlapsDate(reservation, reservationRepository);

    if(reservation.getId() == null && reservation.getStatus() == null)
      reservation.setStatus(Reservation.Status.PENDING);

    reservation.setTimeInput(ZonedDateTime.now());
    reservation = reservationRepository.save(reservation);
    updateLogService.create(reservation, isNew);
    return reservationMapper.reservationToReservationDTO(reservation);
  }

  public ReservationDTO save(ReservationDTO reservationDTO) {
    return save(reservationDTO, false);
  }

  /**
   *  get all the reservations.
   *  @return the list of entities
   */
  @Transactional(readOnly = true)
  public Page<Reservation> findAll(Pageable pageable) {
    log.debug("Request to get all Reservations");
    Page<Reservation> result = reservationRepository.findAll(pageable);
    return result;
  }

  @Override
  public Page<Reservation> findAllCurrentUser(Pageable pageable) {
    return reservationRepository.findByUserIsCurrentUser(pageable);
  }

  /**
   *  get one reservation by id.
   *  @return the entity
   */
  @Transactional(readOnly = true)
  public ReservationDTO findOne(Long id) {
    log.debug("Request to get Reservation : {}", id);
    Reservation reservation = reservationRepository.findOne(id);
    ReservationDTO reservationDTO = reservationMapper.reservationToReservationDTO(reservation);
    return reservationDTO;
  }

  /**
   *  delete the  reservation by id.
   */
  public void delete(Long id) {
    log.debug("Request to delete Reservation : {}", id);
    updateLogService.create(id, Reservation.type);
    reservationRepository.delete(id);
  }

  public void deleteAllByRoomId(Long roomId) {
    List<Reservation> list = reservationRepository.findAllByRoomId(roomId);
    list.forEach(r -> {
      String roomName = r.getRoom().getRoomName();
      r.setRoomUpdateInfo("Please Choose Another Room!");
      r.setRoom(null);
    });
    reservationRepository.save(list);
  }

  public Page<Reservation> findAllByStatus(Integer status, Pageable pageable) {
    return reservationRepository.findAllByStatus(status, pageable);
  }
  public Page<Reservation> findAllByStatus(String userLogin, Integer status, Pageable pageable) {
    return reservationRepository.findAllByUserLoginIgnoreCaseAndStatus(userLogin, status, pageable);
  }

  public Page<Reservation> findAllByUserLogin(String userLogin, Pageable pageable) {
    log.debug("Request to get All Reservation Filtered By UserLogin : {}", userLogin);
    return reservationRepository.findAllByUserLoginContainingIgnoreCase(userLogin, pageable);
  }

  public ReservationDTO findOneByRoomId(Long roomId) {
    Reservation reservation = reservationRepository.findOneByRoomId(roomId);
    return reservationMapper.reservationToReservationDTO(reservation);
  }

  public Page<Reservation> findDataWithInfoNotNull(String userLogin, Pageable pageable) {
    return reservationRepository.findAllByUserLoginIgnoreCaseAndRoomUpdateInfoNotNull(userLogin, pageable);
  }

  public Page<Reservation> findDataByKeyword(String searchType, String key, Pageable pageable) {
    Page<Reservation> page = findAll(pageable);
    switch (searchType) {
      case "room": {
        Optional<Room> room = roomService.findOneByRoomNameIgnoreCase(key);
        if(room.isPresent())
          page = reservationRepository.findAllByRoomId(room.get().getId(), pageable);
        break;
      }
      case "login": {
        Page<Reservation> t = reservationRepository.findAllByUserLoginContainingIgnoreCase(key.trim(), pageable);
        if(t.getSize() != 0) page = t;
        break;
      }
      default: {
        break;
      }
    }
    return page;
  }
  public Page<Reservation> findDataByKeyword(String searchType, String userLogin, String key, Pageable pageable) {
    Page<Reservation> page = findAll(pageable);
    if("room".equals(searchType)) {
      Optional<Room> room = roomService.findOneByRoomNameIgnoreCase(key);
      if(room.isPresent())
        page = reservationRepository.findAllByUserLoginContainingIgnoreCaseAndRoomId(userLogin, room.get().getId(), pageable);
    }
    return page;
  }

  public List<ReservationDTO> getAllByRoom(Long roomId) {
    List<Reservation> list = reservationRepository.findAllByRoomId(roomId);
    List<ReservationDTO> listDTO = new ArrayList<>();
    list.forEach(data -> listDTO.add(reservationMapper.reservationToReservationDTO(data)));
    return listDTO;
  }

  public List<Reservation> findByTimeStartGreaterAndLess(ZonedDateTime start, ZonedDateTime end) {
    return reservationRepository.findAllByUsageTimeStartGreaterThanEqualAndUsageTimeStartLessThanEqual(start, end);
  }
  public List<Reservation> findByTimeEndGreaterAndLess(ZonedDateTime start, ZonedDateTime end) {
    return reservationRepository.findAllByUsageTimeEndGreaterThanEqualAndUsageTimeEndLessThanEqual(start, end);
  }

  public Page<Reservation> findAllByUser(String userLogin, Pageable pageable) {
    return reservationRepository.findByUserIsCurrentUser(pageable);
  }
}
