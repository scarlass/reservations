package app.service.impl;

import app.domain.Reservation;
import app.service.UpdateLogService;
import app.domain.UpdateLog;
import app.repository.UpdateLogRepository;
import app.web.rest.dto.UpdateLogDTO;
import app.web.rest.mapper.UpdateLogMapper;
import org.hibernate.sql.Update;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Service Implementation for managing UpdateLog.
 */
@Service
@Transactional
public class UpdateLogServiceImpl implements UpdateLogService {

  private final Logger log = LoggerFactory.getLogger(UpdateLogServiceImpl.class);

  @Inject
  private UpdateLogRepository updateLogRepository;

  @Inject
  private UpdateLogMapper updateLogMapper;

  /**
   * Save a updateLog.
   * @return the persisted entity
   */
  public UpdateLogDTO save(UpdateLogDTO updateLogDTO) {
    log.debug("Request to save UpdateLog : {}", updateLogDTO);
    UpdateLog updateLog = updateLogMapper.updateLogDTOToUpdateLog(updateLogDTO);
    updateLog = updateLogRepository.save(updateLog);
    UpdateLogDTO result = updateLogMapper.updateLogToUpdateLogDTO(updateLog);
    return result;
  }

  /**
   *  get all the updateLogs.
   *  @return the list of entities
   */
  @Transactional(readOnly = true)
  public Page<UpdateLog> findAll(Pageable pageable) {
    log.debug("Request to get all UpdateLogs");
    Page<UpdateLog> result = updateLogRepository.findAll(pageable);
    return result;
  }

  /**
   *  get one updateLog by id.
   *  @return the entity
   */
  @Transactional(readOnly = true)
  public UpdateLogDTO findOne(Long id) {
    log.debug("Request to get UpdateLog : {}", id);
    UpdateLog updateLog = updateLogRepository.findOne(id);
    UpdateLogDTO updateLogDTO = updateLogMapper.updateLogToUpdateLogDTO(updateLog);
    return updateLogDTO;
  }

  /**
   *  delete the  updateLog by id.
   */
  public void delete(Long id) {
    log.debug("Request to delete UpdateLog : {}", id);
    updateLogRepository.delete(id);
  }

  public <T extends Serializable> T create(T data, boolean newlyCreate) {
    UpdateLog updateLog = new UpdateLog();

    updateLog.setTimeInput(ZonedDateTime.now());
    updateLog.createUpdateLog(data, newlyCreate ? UpdateLog.MessageType.CREATE : UpdateLog.MessageType.UPDATED);

    System.out.print("\n\t" + updateLogMapper.updateLogToUpdateLogDTO(updateLog) + "\n");

    return (T) updateLogRepository.save(updateLog);
  }

  public UpdateLog create(Long id, Integer entityType) {
    deleteByEntityId(id, entityType);
    UpdateLog updateLog = new UpdateLog();
    Integer messageType = UpdateLog.MessageType.DELETE;

    updateLog.setTimeInput(ZonedDateTime.now());
    updateLog.deleteLog(entityType, messageType, id);
    return updateLogRepository.save(updateLog);
  }

  public void deleteByEntityId(Long id, Integer entityType) {
    List<UpdateLog> updateLogs = null;
    switch (Objects.requireNonNull(UpdateLog.EntityType.type(entityType))) {
      case "Reservation":
        updateLogs = updateLogRepository.findAllByReservationId(id);
        if(updateLogs != null && updateLogs.size() > 0)
          updateLogs.forEach(u -> u.setReservation(null));
          updateLogRepository.save(updateLogs);
        break;
      case "Room":
        updateLogs = updateLogRepository.findAllByRoomId(id);
        if(updateLogs != null && updateLogs.size() > 0)
          updateLogs.forEach(u -> u.setRoom(null));
          updateLogRepository.save(updateLogs);
        break;
      case "Item":
        updateLogs = updateLogRepository.findAllByItemId(id);
        if(updateLogs != null && updateLogs.size() > 0)
          updateLogs.forEach(u -> u.setItem(null));
          updateLogRepository.save(updateLogs);
        break;
      default: break;
    }

  }
}
