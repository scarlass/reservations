package app.service.impl;

import app.domain.Item;
import app.service.ItemService;
import app.service.ReservationService;
import app.service.RoomService;
import app.domain.Room;
import app.repository.RoomRepository;
import app.service.UpdateLogService;
import app.web.rest.dto.RoomDTO;
import app.web.rest.mapper.RoomMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Service Implementation for managing Room.
 */
@Service
@Transactional
public class RoomServiceImpl implements RoomService {

  private final Logger log = LoggerFactory.getLogger(RoomServiceImpl.class);

  @Inject
  private RoomRepository roomRepository;

  @Inject
  private RoomMapper roomMapper;

  @Inject
  private ReservationService reservationService;

  @Inject
  private UpdateLogService updateLogService;

  @Inject
  private ItemService itemService;
  /**
   * Save a room.
   *
   * @return the persisted entity
   */
  public RoomDTO save(RoomDTO roomDTO) {
    log.debug("Request to save Room : {}", roomDTO);
    Room room = roomMapper.roomDTOToRoom(roomDTO);
    final boolean isNew = room.getId() == null;

    Optional<Room> sameName = roomRepository.findOneByRoomName(room.getRoomName());
    if (isNew && sameName.isPresent()) {
      log.debug("\n\n\t" + sameName + "\n\n");
      throw new IllegalArgumentException("Declared Name!.");
    }

    room.setTimeInput(ZonedDateTime.now());
    room = roomRepository.save(room);
    updateLogService.create(room, isNew);
    return roomMapper.roomToRoomDTO(room);
  }

  /**
   * get all the rooms.
   *
   * @return the list of entities
   */
  @Transactional(readOnly = true)
  public Page<Room> findAll(Pageable pageable) {
    log.debug("Request to get all Rooms");
    return roomRepository.findAll(pageable);
  }

  /**
   * get one room by id.
   *
   * @return the entity
   */
  @Transactional(readOnly = true)
  public RoomDTO findOne(Long id) {
    log.debug("Request to get Room : {}", id);
    Room room = roomRepository.findOneWithEagerRelationships(id);
    return roomMapper.roomToRoomDTO(room);
  }

  /**
   * delete the room by id.
   */
  public void delete(Long id) {
    log.debug("Request to delete Room : {}", id);
    updateLogService.create(id, Room.type);
    reservationService.deleteAllByRoomId(id);
    roomRepository.delete(id);
  }

  public Page<Room> findPageByKeyword(String searchType, String roomName, Pageable pageable) {
    log.debug("Request to get all By: " + searchType);
    switch (searchType) {
      case "RoomName":
        return roomRepository.findAllByRoomNameContainingIgnoreCase(roomName.trim(), pageable);
      default:
        return findAll(pageable);
    }
  }

  public Optional<Room> findOneByRoomNameIgnoreCase(String roomName) {
    return roomRepository.findOneByRoomNameContainingIgnoreCase(roomName);
  }

  public void deleteByListedItemContainsItemId(Long itemId) {
    Item item = itemService.findOneById(itemId);
    List<Room> list = roomRepository.findAllByItems(item);

    log.debug("\n\tget from room by item id: {}\n", list);

    for(Room l : list) {
      l.getListedItemss().remove(item);
      roomRepository.save(l);
    }
  }
}
