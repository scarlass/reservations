package app.service;

import app.domain.Item;
import app.web.rest.dto.ItemDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Item.
 */
public interface ItemService {

    /**
     * Save a item.
     * @return the persisted entity
     */
    public ItemDTO save(ItemDTO itemDTO);

    /**
     *  get all the items.
     *  @return the list of entities
     */
    public Page<Item> findAll(Pageable pageable);

    /**
     *  get the "id" item.
     *  @return the entity
     */
    public ItemDTO findOne(Long id);

    public Item findOneById(Long itemId);

    /**
     *  delete the "id" item.
     */
    public void delete(Long id);
    public Page<Item> findItemNameByKeyword(String searchType, String key, Pageable pageable);

}
