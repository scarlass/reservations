package app.service;

import app.domain.Reservation;
import app.web.rest.dto.ReservationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Reservation.
 */
public interface ReservationService {

  /**
   *  get all the reservations.
   *  @return the list of entities
   */
  public Page<Reservation> findAll(Pageable pageable);
  public Page<Reservation> findAllCurrentUser(Pageable pageable);

  /**
   * Save a reservation.
   * @return the persisted entity
   */
  public ReservationDTO save(ReservationDTO reservationDTO, boolean skipDateChecks);
  public ReservationDTO save(ReservationDTO reservationDTO);

  /**
   *  get the "id" reservation.
   *  @return the entity
   */
  public ReservationDTO findOne(Long id);

  /**
     *  delete the "id" reservation.
     */
  public void delete(Long id);
  public void deleteAllByRoomId(Long roomId);

  public Page<Reservation> findDataByKeyword(String searchType, String userLogin, String key, Pageable pageable);
  public Page<Reservation> findDataByKeyword(String searchType, String key, Pageable pageable);

  public Page<Reservation> findAllByStatus(Integer status, Pageable pageable);
  public Page<Reservation> findAllByStatus(String userLogin, Integer status, Pageable pageable);

  public List<Reservation> findByTimeStartGreaterAndLess(ZonedDateTime start, ZonedDateTime end);
  public List<Reservation> findByTimeEndGreaterAndLess(ZonedDateTime start, ZonedDateTime end);
}
