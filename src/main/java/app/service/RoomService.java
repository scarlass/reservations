package app.service;

import app.domain.Room;
import app.web.rest.dto.RoomDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Room.
 */
public interface RoomService {

  /**
   * Save a room.
   *
   * @return the persisted entity
   */
  public RoomDTO save(RoomDTO roomDTO);

  /**
   * get all the rooms.
   *
   * @return the list of entities
   */
  public Page<Room> findAll(Pageable pageable);

  /**
   * get the "id" room.
   *
   * @return the entity
   */
  public RoomDTO findOne(Long id);

  /**
   * delete the "id" room.
   */
  public void delete(Long id);


  public Page<Room> findPageByKeyword(String searchType, String roomName, Pageable pageable);
  public Optional<Room> findOneByRoomNameIgnoreCase(String roomName);
  public void deleteByListedItemContainsItemId(Long itemId);
}
