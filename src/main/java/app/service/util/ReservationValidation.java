package app.service.util;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.domain.Reservation;

public class ReservationValidation {
  private final Logger log = LoggerFactory.getLogger(ReservationValidation.class);

  private Reservation reservation;
  private ZonedDateTime startTime;
  private ZonedDateTime endTime;
  private Long resId = null;
  private Long roomId = null;
  private DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("E, dd MM yyy, hh:mm a");

  private String error = null;
  private boolean iserror = false;
  private boolean skip = false;

  public ReservationValidation(Reservation reservation) {
    this.reservation = reservation;
    this.startTime = reservation.getUsageTimeStart();
    this.endTime = reservation.getUsageTimeEnd();
    this.resId = reservation.getId();
    this.roomId = reservation.getRoom().getId();
  }

  public void checkSameCustomer(List<Reservation> reservationSameCustomer) {
    for (Reservation itr : reservationSameCustomer) {
      if (resId != null && itr.getId().equals(resId)) {
        this.skip = true;
        return;
      }

      ZonedDateTime itrStart = itr.getUsageTimeStart();
      ZonedDateTime itrEnd = itr.getUsageTimeEnd();

      boolean startInRange = Utils.dateIsBetween(this.startTime, itrStart, itrEnd);
      boolean endInRange = Utils.dateIsBetween(this.endTime, itrStart, itrEnd);

      if (startInRange || endInRange) {
        this.iserror = true;
        this.error = "The Customer already has reservation at different room, try to use different time";
        break;
      }

    }

  }

  public void checkSameRoom(List<Reservation> reservationSameRoom, boolean skipRoomCheck) {
    if(reservationSameRoom == null) {
      return;
    }

    for (Reservation itr : reservationSameRoom) {
      if (skipRoomCheck && itr.getRoom().getId().equals(roomId)) {
        continue;
      }
      ZonedDateTime itrStart = itr.getUsageTimeStart();
      ZonedDateTime itrEnd = itr.getUsageTimeEnd();

      boolean startInRange = Utils.dateIsBetween(this.startTime, itrStart, itrEnd);
      boolean endInRange = Utils.dateIsBetween(this.endTime, itrStart, itrEnd);

      if (startInRange || endInRange) {
        this.iserror = true;
        this.error = "\"" + itr.getRoom().getRoomName() + "\" already reserved between: \nStart: "
            + itrStart.format(this.dateFormat) + "\nEnd: " + itrEnd.format(this.dateFormat);
        break;
      }
    }
  }

  public String getError() {
    final String get = this.error;
    this.error = null;
    return get;
  }

  public boolean hasError() {
    final boolean get = this.iserror;
    this.iserror = false;
    return get;
  }

  public boolean isSkipCheckRoom() {
    final boolean get = this.skip;
    this.skip = false;
    return get;
  }

}
