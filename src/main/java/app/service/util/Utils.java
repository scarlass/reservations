package app.service.util;

import app.domain.Reservation;
import app.repository.ReservationRepository;
import app.service.ReservationService;

import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public final class Utils {
  private Utils() {}

  static public boolean dateIsBetween(ZonedDateTime target, ZonedDateTime startDate, ZonedDateTime endDate) {
    boolean start = target.compareTo(startDate) >= 0;
    boolean end = target.compareTo(endDate) <= 0;
    boolean result = start && end;

    System.out.println("Date is Used ? " + result + "\n");
    return result;
  }

  static public void checkOverlapsDate(Reservation reservation, ReservationRepository reservationRepository) {
    if(reservation == null) return;
    System.out.println("\n\tChecking Used Date Time: " + "\n");

    ZonedDateTime start = reservation.getUsageTimeStart();
    ZonedDateTime end = reservation.getUsageTimeEnd();

    List<Reservation> listStart = reservationRepository.findAllByUsageTimeStartGreaterThanEqualAndUsageTimeStartLessThanEqual(start, end);
    List<Reservation> listEnd = reservationRepository.findAllByUsageTimeEndGreaterThanEqualAndUsageTimeEndLessThanEqual(start, end);
    boolean hasStart = listStart.size() != 0;
    boolean hasEnd = listEnd.size() != 0;

    if(hasStart || hasEnd) {
      if(hasStart) deepCheck(listStart, reservation);
      else deepCheck(listEnd, reservation);
    }
  }

  static private <T extends List<Reservation>> void deepCheck(T lists, Reservation reservation) {
    Long resId = reservation.getId();
    Long roomId = reservation.getRoom().getId();
    String userLogin = reservation.getUserLogin();

    String reason = null;
    Reservation sameData = null;

    for(Reservation list: lists) {
      Long listId = list.getId();
      Long listRoomId = list.getRoom().getId();
      String listUser = list.getUserLogin();

      if(resId == null) {
        if(listUser.equals(userLogin)) {
          reason = "user";
          sameData = list;
          break;
        }
        else if(listRoomId.equals(roomId)) {
          reason = "room";
          sameData = list;
          break;
        }
      }
      else {
        if(!resId.equals(listId) && listRoomId.equals(roomId)) {
          reason = "room";
          sameData = list;
          break;
        }
      }
    }

    if(sameData != null) {
      DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("E, dd MM yyy, hh:mm");
      String usedRoom = sameData.getRoom().getRoomName();
      ZonedDateTime usedStart = sameData.getUsageTimeStart();
      ZonedDateTime usedEnd = sameData.getUsageTimeEnd();

      switch (reason) {
        case "user": {
          throw new IllegalArgumentException(
            "You already has reservation at different room, try to use different time"
          );
        }
        case "room": {
          throw new IllegalArgumentException(
            "\"" + usedRoom + "\" already reserved between: \nStart: "
              + usedStart.format(dateFormat) + "\nEnd: " + usedEnd.format(dateFormat)
          );
        }
        default: break;
      }
    }

  }

}
