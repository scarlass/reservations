package app.service;

import app.domain.Item;
import app.domain.Reservation;
import app.domain.Room;
import app.domain.UpdateLog;
import app.web.rest.dto.ItemDTO;
import app.web.rest.dto.ReservationDTO;
import app.web.rest.dto.RoomDTO;
import app.web.rest.dto.UpdateLogDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing UpdateLog.
 */
public interface UpdateLogService {

    /**
     * Save a updateLog.
     * @return the persisted entity
     */
    public UpdateLogDTO save(UpdateLogDTO updateLogDTO);

    /**
     *  get all the updateLogs.
     *  @return the list of entities
     */
    public Page<UpdateLog> findAll(Pageable pageable);

    /**
     *  get the "id" updateLog.
     *  @return the entity
     */
    public UpdateLogDTO findOne(Long id);

    /**
     *  delete the "id" updateLog.
     */
    public void delete(Long id);

  public <T extends Serializable> T create(T data, boolean newlyCreate);

  public UpdateLog create(Long id, Integer entityType);

  public void deleteByEntityId(Long id, Integer entityType);
}
